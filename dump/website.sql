# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.26)
# Database: website
# Generation Time: 2020-02-20 09:09:49 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table albums
# ------------------------------------------------------------

DROP TABLE IF EXISTS `albums`;

CREATE TABLE `albums` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `release_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `artist_id` int(10) unsigned NOT NULL DEFAULT '0',
  `spotify_popularity` tinyint(4) DEFAULT NULL,
  `fully_scraped` tinyint(1) NOT NULL DEFAULT '0',
  `temp_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `auto_update` tinyint(1) NOT NULL DEFAULT '1',
  `local_only` tinyint(1) NOT NULL DEFAULT '0',
  `spotify_id` char(22) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `artist_type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'App\\Artist',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `albums_spotify_id_unique` (`spotify_id`),
  KEY `albums_release_date_index` (`release_date`),
  KEY `albums_artist_id_index` (`artist_id`),
  KEY `albums_spotify_popularity_index` (`spotify_popularity`),
  KEY `albums_temp_id_index` (`temp_id`),
  KEY `albums_fully_scraped_index` (`fully_scraped`),
  KEY `albums_views_index` (`views`),
  KEY `albums_artist_type_index` (`artist_type`),
  KEY `albums_local_only_index` (`local_only`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table artist_bios
# ------------------------------------------------------------

DROP TABLE IF EXISTS `artist_bios`;

CREATE TABLE `artist_bios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `artist_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `artist_bios_artist_id_unique` (`artist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `artist_bios` WRITE;
/*!40000 ALTER TABLE `artist_bios` DISABLE KEYS */;

INSERT INTO `artist_bios` (`id`, `content`, `artist_id`, `created_at`, `updated_at`)
VALUES
	(1,NULL,1,'2020-02-07 08:23:49','2020-02-07 08:23:49');

/*!40000 ALTER TABLE `artist_bios` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table artist_track
# ------------------------------------------------------------

DROP TABLE IF EXISTS `artist_track`;

CREATE TABLE `artist_track` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `artist_id` int(10) unsigned NOT NULL,
  `track_id` int(10) unsigned NOT NULL,
  `artist_type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'App\\Artist',
  `primary` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `artist_track_artist_id_track_id_unique` (`artist_id`,`track_id`),
  KEY `artist_track_artist_id_index` (`artist_id`),
  KEY `artist_track_track_id_index` (`track_id`),
  KEY `artist_track_artist_type_index` (`artist_type`),
  KEY `artist_track_primary_index` (`primary`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `artist_track` WRITE;
/*!40000 ALTER TABLE `artist_track` DISABLE KEYS */;

INSERT INTO `artist_track` (`id`, `artist_id`, `track_id`, `artist_type`, `primary`)
VALUES
	(1,1,1,'App\\Artist',1),
	(2,2,2,'App\\Artist',2);

/*!40000 ALTER TABLE `artist_track` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table artists
# ------------------------------------------------------------

DROP TABLE IF EXISTS `artists`;

CREATE TABLE `artists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spotify_followers` int(10) unsigned DEFAULT NULL,
  `spotify_popularity` tinyint(3) unsigned DEFAULT NULL,
  `image_small` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_large` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fully_scraped` tinyint(1) NOT NULL DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT NULL,
  `bio_legacy` text COLLATE utf8mb4_unicode_ci,
  `wiki_image_large` mediumtext COLLATE utf8mb4_unicode_ci,
  `wiki_image_small` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `auto_update` tinyint(1) NOT NULL DEFAULT '1',
  `spotify_id` char(22) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `artists_spotify_id_unique` (`spotify_id`),
  KEY `artists_spotify_popularity_index` (`spotify_popularity`),
  KEY `artists_fully_scraped_index` (`fully_scraped`),
  KEY `artists_views_index` (`views`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `artists` WRITE;
/*!40000 ALTER TABLE `artists` DISABLE KEYS */;

INSERT INTO `artists` (`id`, `name`, `spotify_followers`, `spotify_popularity`, `image_small`, `image_large`, `fully_scraped`, `updated_at`, `bio_legacy`, `wiki_image_large`, `wiki_image_small`, `created_at`, `views`, `auto_update`, `spotify_id`)
VALUES
	(1,'arijit',NULL,50,NULL,NULL,0,'2020-02-09 15:48:34',NULL,NULL,NULL,'2020-02-07 08:23:49',2,1,NULL),
	(2,'arjitd2',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,0,1,NULL),
	(3,'arijit',NULL,50,NULL,NULL,0,'2020-02-09 15:48:34',NULL,NULL,NULL,'2020-02-07 08:23:49',2,1,NULL);

/*!40000 ALTER TABLE `artists` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table billing_plans
# ------------------------------------------------------------

DROP TABLE IF EXISTS `billing_plans`;

CREATE TABLE `billing_plans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(13,2) DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_symbol` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '$',
  `interval` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'month',
  `interval_count` int(11) NOT NULL DEFAULT '1',
  `parent_id` int(11) DEFAULT NULL,
  `legacy_permissions` text COLLATE utf8mb4_unicode_ci,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paypal_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recommended` tinyint(1) NOT NULL DEFAULT '0',
  `free` tinyint(1) NOT NULL DEFAULT '0',
  `show_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `features` text COLLATE utf8mb4_unicode_ci,
  `position` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `available_space` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table bio_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bio_images`;

CREATE TABLE `bio_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `artist_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bio_images_artist_id_index` (`artist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table channelables
# ------------------------------------------------------------

DROP TABLE IF EXISTS `channelables`;

CREATE TABLE `channelables` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) NOT NULL,
  `channelable_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `channelable_id` int(11) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `channelables_channelable_type_channelable_id_channel_id_unique` (`channelable_type`,`channelable_id`,`channel_id`),
  KEY `channelables_channel_id_index` (`channel_id`),
  KEY `channelables_channelable_type_index` (`channelable_type`),
  KEY `channelables_channelable_id_index` (`channelable_id`),
  KEY `channelables_order_index` (`order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `channelables` WRITE;
/*!40000 ALTER TABLE `channelables` DISABLE KEYS */;

INSERT INTO `channelables` (`id`, `channel_id`, `channelable_type`, `channelable_id`, `order`)
VALUES
	(1,5,'App\\Channel',1,1),
	(2,5,'App\\Channel',4,2),
	(3,5,'App\\Channel',2,3),
	(4,5,'App\\Channel',3,4);

/*!40000 ALTER TABLE `channelables` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table channels
# ------------------------------------------------------------

DROP TABLE IF EXISTS `channels`;

CREATE TABLE `channels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'mixed',
  `auto_update` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `layout` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hide_title` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `channels_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `channels` WRITE;
/*!40000 ALTER TABLE `channels` DISABLE KEYS */;

INSERT INTO `channels` (`id`, `name`, `slug`, `content_type`, `auto_update`, `layout`, `hide_title`, `user_id`, `seo_title`, `seo_description`, `created_at`, `updated_at`)
VALUES
	(1,'Popular Albums','popular-albums','album','local:album:top','carousel',0,1,'Popular Albums','Most popular albums from hottest artists today.','2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(2,'New Releases','new-releases','album','local:album:new','carousel',0,1,'Latest Releases','Browse and listen to newest releases from popular artists.','2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(3,'Genres','genres','genre','local:genre:top','grid',0,1,'Popular Genres','Browse popular genres to discover new music.','2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(4,'Popular Tracks','popular-tracks','track','local:track:top','trackTable',0,1,'Popular Tracks','Global Top 50 chart of most popular songs.','2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(5,'Discover','discover','channel',NULL,NULL,1,1,'BeMusic - Listen to music for free','Find and listen to millions of songs, albums and artists, all completely free on BeMusic.','2020-02-06 16:43:28','2020-02-06 16:43:28');

/*!40000 ALTER TABLE `channels` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `commentable_id` int(10) unsigned NOT NULL,
  `commentable_type` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `comments_parent_id_index` (`parent_id`),
  KEY `comments_path_index` (`path`),
  KEY `comments_user_id_index` (`user_id`),
  KEY `comments_commentable_id_index` (`commentable_id`),
  KEY `comments_commentable_type_index` (`commentable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table css_themes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `css_themes`;

CREATE TABLE `css_themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_dark` tinyint(1) NOT NULL DEFAULT '0',
  `default_light` tinyint(1) NOT NULL DEFAULT '0',
  `default_dark` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `colors` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `css_themes_name_unique` (`name`),
  KEY `css_themes_default_light_index` (`default_light`),
  KEY `css_themes_default_dark_index` (`default_dark`),
  KEY `css_themes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `css_themes` WRITE;
/*!40000 ALTER TABLE `css_themes` DISABLE KEYS */;

INSERT INTO `css_themes` (`id`, `name`, `is_dark`, `default_light`, `default_dark`, `user_id`, `colors`, `created_at`, `updated_at`)
VALUES
	(1,'Dark',1,0,1,1,'{\"--be-primary-lighter\":\"#333333\",\"--be-primary-default\":\"#242424\",\"--be-primary-darker\":\"#1e1e1e\",\"--be-accent-default\":\"#689f38\",\"--be-accent-lighter\":\"#B4CF9C\",\"--be-accent-contrast\":\"rgba(255, 255, 255, 1)\",\"--be-accent-emphasis\":\"rgb(180,207,156, 0.1)\",\"--be-foreground-base\":\"#fff\",\"--be-text\":\"#fff\",\"--be-hint-text\":\"rgba(255, 255, 255, 0.5)\",\"--be-secondary-text\":\"rgba(255, 255, 255, 0.7)\",\"--be-label\":\"rgba(255, 255, 255, 0.7)\",\"--be-background\":\"#23232C\",\"--be-background-alternative\":\"#1e1e26\",\"--be-divider-lighter\":\"rgba(255, 255, 255, 0.06)\",\"--be-divider-default\":\"rgba(255, 255, 255, 0.12)\",\"--be-disabled-button-text\":\"rgba(255, 255, 255, 0.3)\",\"--be-disabled-toggle\":\"#000\",\"--be-chip\":\"#353543\",\"--be-hover\":\"rgba(255, 255, 255, 0.04)\",\"--be-selected-button\":\"#212121\",\"--be-disabled-button\":\"rgba(255, 255, 255, 0.12)\",\"--be-raised-button\":\"#2a2a35\",\"--be-backdrop\":\"#BDBDBD\",\"--be-link\":\"#c5cae9\"}','2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(2,'Light',0,1,0,1,'{\"--be-primary-lighter\":\"#37474f\",\"--be-primary-default\":\"#263238\",\"--be-primary-darker\":\"#1C262B\",\"--be-accent-default\":\"#689f38\",\"--be-accent-lighter\":\"#B4CF9C\",\"--be-accent-contrast\":\"rgba(255, 255, 255, 1)\",\"--be-accent-emphasis\":\"rgb(180,207,156, 0.1)\",\"--be-background\":\"rgb(255, 255, 255)\",\"--be-background-alternative\":\"rgb(250, 250, 250)\",\"--be-foreground-base\":\"black\",\"--be-text\":\"rgba(0, 0, 0, 0.87)\",\"--be-hint-text\":\"rgba(0, 0, 0, 0.38)\",\"--be-secondary-text\":\"rgba(0, 0, 0, 0.54)\",\"--be-label\":\"rgba(0, 0, 0, 0.87)\",\"--be-disabled-button-text\":\"rgba(0, 0, 0, 0.26)\",\"--be-divider-lighter\":\"rgba(0, 0, 0, 0.06)\",\"--be-divider-default\":\"rgba(0, 0, 0, 0.12)\",\"--be-hover\":\"rgba(0,0,0,0.04)\",\"--be-selected-button\":\"rgb(224, 224, 224)\",\"--be-chip\":\"#e0e0e0\",\"--be-link\":\"#3f51b5\",\"--be-backdrop\":\"black\",\"--be-raised-button\":\"#fff\",\"--be-disabled-toggle\":\"rgb(238, 238, 238)\",\"--be-disabled-button\":\"rgba(0, 0, 0, 0.12)\"}','2020-02-06 16:43:28','2020-02-06 16:43:28');

/*!40000 ALTER TABLE `css_themes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table custom_domains
# ------------------------------------------------------------

DROP TABLE IF EXISTS `custom_domains`;

CREATE TABLE `custom_domains` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `host` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `global` tinyint(1) NOT NULL DEFAULT '0',
  `resource_id` int(10) unsigned DEFAULT NULL,
  `resource_type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `custom_domains_host_unique` (`host`),
  KEY `custom_domains_user_id_index` (`user_id`),
  KEY `custom_domains_global_index` (`global`),
  KEY `custom_domains_resource_id_index` (`resource_id`),
  KEY `custom_domains_resource_type_index` (`resource_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table custom_pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `custom_pages`;

CREATE TABLE `custom_pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`),
  KEY `pages_type_index` (`type`),
  KEY `pages_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `custom_pages` WRITE;
/*!40000 ALTER TABLE `custom_pages` DISABLE KEYS */;

INSERT INTO `custom_pages` (`id`, `title`, `body`, `slug`, `meta`, `type`, `created_at`, `updated_at`, `user_id`)
VALUES
	(1,'Privacy Policy','<h1>Example Privacy Policy</h1><p>The standard Lorem Ipsum passage, used since the 1500s\n    \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>\n\n<p>Section 1.10.32 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC\n    \"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\"</p>\n\n<p>1914 translation by H. Rackham\n    \"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?\"</p>\n\n<p>Section 1.10.33 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC\n    \"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.\"</p>\n\n<p>1914 translation by H. Rackham\n    \"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.\"</p>','privacy-policy',NULL,'default','2020-02-06 16:43:28','2020-02-06 16:43:28',NULL),
	(2,'Terms of Service','<h1>Example Terms of Service</h1><p>The standard Lorem Ipsum passage, used since the 1500s\n    \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>\n\n<p>Section 1.10.32 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC\n    \"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\"</p>\n\n<p>1914 translation by H. Rackham\n    \"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?\"</p>\n\n<p>Section 1.10.33 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC\n    \"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.\"</p>\n\n<p>1914 translation by H. Rackham\n    \"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.\"</p>','terms-of-service',NULL,'default','2020-02-06 16:43:28','2020-02-06 16:43:28',NULL),
	(3,'About Us','<h1>Example About Us</h1><p>The standard Lorem Ipsum passage, used since the 1500s\n    \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>\n\n<p>Section 1.10.32 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC\n    \"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\"</p>\n\n<p>1914 translation by H. Rackham\n    \"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?\"</p>\n\n<p>Section 1.10.33 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC\n    \"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.\"</p>\n\n<p>1914 translation by H. Rackham\n    \"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.\"</p>','about-us',NULL,'default','2020-02-06 16:43:28','2020-02-06 16:43:28',NULL);

/*!40000 ALTER TABLE `custom_pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table file_entries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `file_entries`;

CREATE TABLE `file_entries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` bigint(20) unsigned DEFAULT NULL,
  `mime` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '0',
  `public_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `description` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `preview_token` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbnail` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uploads_file_name_unique` (`file_name`),
  KEY `uploads_name_index` (`name`),
  KEY `uploads_user_id_index` (`user_id`),
  KEY `uploads_public_index` (`public`),
  KEY `file_entries_updated_at_index` (`updated_at`),
  KEY `file_entries_parent_id_index` (`parent_id`),
  KEY `file_entries_type_index` (`type`),
  KEY `file_entries_deleted_at_index` (`deleted_at`),
  KEY `file_entries_user_id_index` (`user_id`),
  KEY `file_entries_path_index` (`path`),
  KEY `file_entries_description_index` (`description`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table file_entry_models
# ------------------------------------------------------------

DROP TABLE IF EXISTS `file_entry_models`;

CREATE TABLE `file_entry_models` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_entry_id` int(10) unsigned NOT NULL,
  `model_id` int(10) unsigned NOT NULL,
  `model_type` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uploadables_upload_id_uploadable_id_uploadable_type_unique` (`file_entry_id`,`model_id`,`model_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table follows
# ------------------------------------------------------------

DROP TABLE IF EXISTS `follows`;

CREATE TABLE `follows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `follower_id` int(11) NOT NULL,
  `followed_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `follows_follower_id_followed_id_unique` (`follower_id`,`followed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table genreables
# ------------------------------------------------------------

DROP TABLE IF EXISTS `genreables`;

CREATE TABLE `genreables` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `genre_id` int(11) NOT NULL,
  `genreable_id` int(11) NOT NULL,
  `genreable_type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'App\\Artist',
  PRIMARY KEY (`id`),
  UNIQUE KEY `genre_artist_genreable_id_genreable_type_genre_id_unique` (`genreable_id`,`genreable_type`,`genre_id`),
  KEY `genre_artist_genreable_type_index` (`genreable_type`),
  KEY `genreables_genreable_id_index` (`genreable_id`),
  KEY `genreables_genre_id_index` (`genre_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table genres
# ------------------------------------------------------------

DROP TABLE IF EXISTS `genres`;

CREATE TABLE `genres` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `popularity` int(10) unsigned DEFAULT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `genres_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table invoices
# ------------------------------------------------------------

DROP TABLE IF EXISTS `invoices`;

CREATE TABLE `invoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subscription_id` int(11) NOT NULL,
  `paid` tinyint(1) NOT NULL,
  `uuid` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoices_subscription_id_index` (`subscription_id`),
  KEY `invoices_uuid_index` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table jobs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jobs`;

CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table likes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `likes`;

CREATE TABLE `likes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `likeable_id` int(10) unsigned NOT NULL,
  `likeable_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'AppTrack',
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `likes_likeable_id_likeable_type_user_id_unique` (`likeable_id`,`likeable_type`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `likes` WRITE;
/*!40000 ALTER TABLE `likes` DISABLE KEYS */;

INSERT INTO `likes` (`id`, `likeable_id`, `likeable_type`, `user_id`, `created_at`, `updated_at`)
VALUES
	(1,1,'App\\Track',2,'2020-02-09 15:48:39',NULL);

/*!40000 ALTER TABLE `likes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table localizations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `localizations`;

CREATE TABLE `localizations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `localizations_name_index` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `localizations` WRITE;
/*!40000 ALTER TABLE `localizations` DISABLE KEYS */;

INSERT INTO `localizations` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'english','2020-02-06 16:43:28','2020-02-06 16:43:28');

/*!40000 ALTER TABLE `localizations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table lyrics
# ------------------------------------------------------------

DROP TABLE IF EXISTS `lyrics`;

CREATE TABLE `lyrics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `track_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lyrics_track_id_unique` (`track_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table mail_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mail_templates`;

CREATE TABLE `mail_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `markdown` tinyint(1) NOT NULL DEFAULT '0',
  `action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mail_templates_file_name_unique` (`file_name`),
  UNIQUE KEY `mail_templates_action_unique` (`action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `mail_templates` WRITE;
/*!40000 ALTER TABLE `mail_templates` DISABLE KEYS */;

INSERT INTO `mail_templates` (`id`, `display_name`, `file_name`, `subject`, `markdown`, `action`, `created_at`, `updated_at`)
VALUES
	(1,'Generic','generic.blade.php','{{EMAIL_SUBJECT}}',0,'generic','2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(2,'Email Confirmation','email-confirmation.blade.php','Confirm your {{SITE_NAME}} account',0,'email_confirmation','2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(3,'Share','share.blade.php','{{DISPLAY_NAME}} shared \'{{ITEM_NAME}}\' with you',0,'share','2020-02-06 16:43:28','2020-02-06 16:43:28');

/*!40000 ALTER TABLE `mail_templates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2015_04_127_156842_create_social_profiles_table',1),
	(4,'2015_04_127_156842_create_users_oauth_table',1),
	(5,'2015_05_29_131549_create_settings_table',1),
	(6,'2015_09_04_155015_create_artists_table',1),
	(7,'2015_09_06_161342_create_albums_table',1),
	(8,'2015_09_06_161348_create_tracks_table',1),
	(9,'2015_09_11_145318_create_similar_artists_table',1),
	(10,'2015_09_17_135717_create_track_user_table',1),
	(11,'2015_09_26_124652_create_playlists_table',1),
	(12,'2015_09_26_131215_create_playlist_track_table',1),
	(13,'2015_09_26_135719_create_playlist_user_table',1),
	(14,'2015_10_16_135253_create_genres_table',1),
	(15,'2015_10_16_135754_create_genre_artist',1),
	(16,'2015_10_23_164355_create_follows_table',1),
	(17,'2015_11_18_134303_add_temp_id_to_albums',1),
	(18,'2015_11_18_134303_add_temp_id_to_tracks',1),
	(19,'2015_11_19_134203_change_fully_scraped_default',1),
	(20,'2016_03_03_143235_add_position_to_playlist_track_table',1),
	(21,'2016_03_14_143858_add_url_to_tracks_table',1),
	(22,'2016_03_18_142141_add_email_confirmation_to_users_table',1),
	(23,'2016_03_19_140502_add_bio_field_to_artists',1),
	(24,'2016_03_24_148503_add_fully_scraped_index_to_albums_table',1),
	(25,'2016_03_24_148503_add_fully_scraped_index_to_artists_table',1),
	(26,'2016_03_24_148503_add_public_index_to_playlists_table',1),
	(27,'2016_03_25_174157_create_sitemap_ids_table',1),
	(28,'2016_03_28_150334_add_image_and_description_to_playlists_table',1),
	(29,'2016_04_10_150419_add_wiki_images_to_artists_table',1),
	(30,'2016_05_02_150429_change_artists_fully_scraped_default',1),
	(31,'2016_05_12_190852_create_tags_table',1),
	(32,'2016_05_12_190958_create_taggables_table',1),
	(33,'2016_05_26_170044_create_uploads_table',1),
	(34,'2016_05_27_143158_create_uploadables_table',1),
	(35,'2016_07_14_153703_create_groups_table',1),
	(36,'2016_07_14_153921_create_user_group_table',1),
	(37,'2017_07_02_120142_create_pages_table',1),
	(38,'2017_07_11_122825_create_localizations_table',1),
	(39,'2017_07_17_135837_create_mail_templates_table',1),
	(40,'2017_08_26_131330_add_private_field_to_settings_table',1),
	(41,'2017_08_26_155115_add_timestamps_to_artists_table',1),
	(42,'2017_09_12_134214_set_playlist_user_owner_column_default_to_zero',1),
	(43,'2017_09_16_155557_create_lyrics_table',1),
	(44,'2017_09_17_144728_add_columns_to_users_table',1),
	(45,'2017_09_17_152854_make_password_column_nullable',1),
	(46,'2017_09_30_152855_make_settings_value_column_nullable',1),
	(47,'2017_10_01_152856_add_views_column_to_artists_table',1),
	(48,'2017_10_01_152857_add_views_column_to_albums_table',1),
	(49,'2017_10_01_152858_add_plays_column_to_tracks_table',1),
	(50,'2017_10_01_152859_add_views_column_to_playlists_table',1),
	(51,'2017_10_01_152897_add_public_column_to_uploads_table',1),
	(52,'2017_12_04_132911_add_avatar_column_to_users_table',1),
	(53,'2018_01_10_140732_create_subscriptions_table',1),
	(54,'2018_01_10_140746_add_billing_to_users_table',1),
	(55,'2018_01_10_161706_create_billing_plans_table',1),
	(56,'2018_06_02_143319_create_user_file_entry_table',1),
	(57,'2018_07_24_113757_add_available_space_to_billing_plans_table',1),
	(58,'2018_07_24_124254_add_available_space_to_users_table',1),
	(59,'2018_07_26_142339_rename_groups_to_roles',1),
	(60,'2018_07_26_142842_rename_user_role_table_columns_to_roles',1),
	(61,'2018_08_07_124200_rename_uploads_to_file_entries',1),
	(62,'2018_08_07_124327_refactor_file_entries_columns',1),
	(63,'2018_08_07_130653_add_folder_path_column_to_file_entries_table',1),
	(64,'2018_08_07_140440_migrate_file_entry_users_to_many_to_many',1),
	(65,'2018_08_15_132225_move_uploads_into_subfolders',1),
	(66,'2018_08_31_104145_rename_uploadables_table',1),
	(67,'2018_08_31_104325_rename_file_entry_models_table_columns',1),
	(68,'2018_09_30_113932_add_auto_update_columns',1),
	(69,'2018_10_01_090754_add_image_and_popularity_columns_to_genres_table',1),
	(70,'2018_11_26_171703_add_type_and_title_columns_to_pages_table',1),
	(71,'2018_12_01_144233_change_unique_index_on_tags_table',1),
	(72,'2019_02_16_150049_delete_old_seo_settings',1),
	(73,'2019_02_24_141457_create_jobs_table',1),
	(74,'2019_03_11_162627_add_preview_token_to_file_entries_table',1),
	(75,'2019_03_12_160803_add_thumbnail_column_to_file_entries_table',1),
	(76,'2019_03_16_161836_add_paypal_id_column_to_billing_plans_table',1),
	(77,'2019_05_14_120930_index_description_column_in_file_entries_table',1),
	(78,'2019_06_08_120504_create_custom_domains_table',1),
	(79,'2019_06_13_140318_add_user_id_column_to_pages_table',1),
	(80,'2019_06_15_114320_rename_pages_table_to_custom_pages',1),
	(81,'2019_06_18_133933_create_permissions_table',1),
	(82,'2019_06_18_134203_create_permissionables_table',1),
	(83,'2019_06_18_135822_rename_permissions_columns',1),
	(84,'2019_06_25_133852_move_inline_permissions_to_separate_table',1),
	(85,'2019_07_08_122001_create_css_themes_table',1),
	(86,'2019_07_20_141752_create_invoices_table',1),
	(87,'2019_08_19_121112_add_global_column_to_custom_domains_table',1),
	(88,'2019_08_20_161927_add_api_token_to_users_table',1),
	(89,'2019_08_20_162247_add_api_tokens_to_existing_users',1),
	(90,'2019_09_13_141123_change_plan_amount_to_float',1),
	(91,'2019_09_17_134818_rename_track_artists_legacy_column',1),
	(92,'2019_09_18_131640_create_artist_track_table',1),
	(93,'2019_09_18_131837_migrate_inline_artists_to_pivot',1),
	(94,'2019_09_19_123359_add_spotify_id_to_tracks_table',1),
	(95,'2019_09_19_161230_add_spotify_id_to_artists_table',1),
	(96,'2019_09_19_161305_add_spotify_id_to_albums_table',1),
	(97,'2019_09_21_115840_remove_track_name_album_name_unique_index',1),
	(98,'2019_09_21_134409_add_timestamps_to_artists_albums_tracks',1),
	(99,'2019_09_22_131629_add_user_id_columns_to_tracks_table',1),
	(100,'2019_09_22_131758_rename_track_user_table_to_liked_tracks',1),
	(101,'2019_09_26_144547_update_albums_to_v2',1),
	(102,'2019_09_30_152608_update_genre_artist_table_to_v2',1),
	(103,'2019_10_02_192908_create_reposts_table',1),
	(104,'2019_10_04_140608_create_user_profiles_table',1),
	(105,'2019_10_04_140907_create_user_links_table',1),
	(106,'2019_10_06_122651_create_channels_table',1),
	(107,'2019_10_06_132717_create_channelables_table',1),
	(108,'2019_10_14_171943_add_index_to_username_column',1),
	(109,'2019_10_15_171019_create_plays_table',1),
	(110,'2019_10_20_143522_create_comments_table',1),
	(111,'2019_10_20_150654_add_columns_to_comments_table',1),
	(112,'2019_10_23_134520_create_notifications_table',1),
	(113,'2019_10_31_154623_artist_bios',1),
	(114,'2019_10_31_154730_create_bio_images_table',1),
	(115,'2019_11_02_151404_move_inline_artist_bios_to_separate_tables',1),
	(116,'2019_11_14_195518_add_name_index_to_artists_table',1),
	(117,'2019_11_15_183635_add_display_name_column_to_genres_table',1),
	(118,'2019_11_16_150409_add_indexes_to_genreables_table',1),
	(119,'2019_11_21_144956_add_resource_id_and_type_to_custom_domains_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table notifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` int(10) unsigned NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_notifiable_id_notifiable_type_index` (`notifiable_id`,`notifiable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table permissionables
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permissionables`;

CREATE TABLE `permissionables` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(11) NOT NULL,
  `permissionable_id` int(11) NOT NULL,
  `permissionable_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `restrictions` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissionable_unique` (`permission_id`,`permissionable_id`,`permissionable_type`),
  KEY `permissionables_permission_id_index` (`permission_id`),
  KEY `permissionables_permissionable_id_index` (`permissionable_id`),
  KEY `permissionables_permissionable_type_index` (`permissionable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `permissionables` WRITE;
/*!40000 ALTER TABLE `permissionables` DISABLE KEYS */;

INSERT INTO `permissionables` (`id`, `permission_id`, `permissionable_id`, `permissionable_type`, `restrictions`)
VALUES
	(1,1,1,'App\\User',NULL),
	(2,9,1,'Common\\Auth\\Roles\\Role','[]'),
	(3,20,1,'Common\\Auth\\Roles\\Role','[]'),
	(4,32,1,'Common\\Auth\\Roles\\Role','[]'),
	(5,35,1,'Common\\Auth\\Roles\\Role','[]'),
	(6,39,1,'Common\\Auth\\Roles\\Role','[]'),
	(7,43,1,'Common\\Auth\\Roles\\Role','[]'),
	(8,48,1,'Common\\Auth\\Roles\\Role','[]'),
	(9,52,1,'Common\\Auth\\Roles\\Role','[]'),
	(10,56,1,'Common\\Auth\\Roles\\Role','[]'),
	(11,60,1,'Common\\Auth\\Roles\\Role','[]'),
	(12,18,2,'Common\\Auth\\Roles\\Role','[]'),
	(13,20,2,'Common\\Auth\\Roles\\Role','[]'),
	(14,24,2,'Common\\Auth\\Roles\\Role','[]'),
	(15,32,2,'Common\\Auth\\Roles\\Role','[]'),
	(16,35,2,'Common\\Auth\\Roles\\Role','[]'),
	(17,39,2,'Common\\Auth\\Roles\\Role','[]'),
	(18,43,2,'Common\\Auth\\Roles\\Role','[]'),
	(19,48,2,'Common\\Auth\\Roles\\Role','[]'),
	(20,52,2,'Common\\Auth\\Roles\\Role','[]'),
	(21,56,2,'Common\\Auth\\Roles\\Role','[]'),
	(22,57,2,'Common\\Auth\\Roles\\Role','[]'),
	(23,60,2,'Common\\Auth\\Roles\\Role','[]');

/*!40000 ALTER TABLE `permissionables` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `restrictions` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `group`, `restrictions`, `created_at`, `updated_at`)
VALUES
	(1,'admin','Super Admin','Give all permissions to user.','admin',NULL,'2020-02-06 16:43:27','2020-02-06 16:43:27'),
	(2,'admin.access','Access Admin','Required in order to access any admin area page.','admin',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(3,'appearance.update','Update Appearance','Allows access to appearance editor.','admin',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(4,'roles.view','View Roles','Allow viewing ALL roles.','roles',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(5,'roles.create','Create Roles','Allow creating new roles.','roles',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(6,'roles.update','Update Roles','Allow updating ALL roles.','roles',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(7,'roles.delete','Delete Roles','Allow deleting ALL roles.','roles',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(8,'reports.view','View Reports','Allows access to analytics page.','analytics',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(9,'custom_pages.view','View Custom Pages','Allow viewing ALL custom pages.','custom_pages',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(10,'custom_pages.create','Create Custom Pages','Allow creating new custom pages.','custom_pages','[{\"name\":\"count\",\"type\":\"number\",\"description\":\"policies.count_description\"}]','2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(11,'custom_pages.update','Update Custom Pages','Allow updating ALL custom pages.','custom_pages',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(12,'custom_pages.delete','Delete Custom Pages','Allow deleting ALL custom pages.','custom_pages',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(13,'custom_domains.view','View Custom Domains','Allow viewing ALL custom domains.','custom_domains',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(14,'custom_domains.create','Create Custom Domains','Allow creating new custom domains.','custom_domains','[{\"name\":\"count\",\"type\":\"number\",\"description\":\"policies.count_description\"}]','2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(15,'custom_domains.update','Update Custom Domains','Allow updating ALL custom domains.','custom_domains',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(16,'custom_domains.delete','Delete Custom Domains','Allow deleting ALL custom domains.','custom_domains',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(17,'files.view','View Files','Allow viewing ALL files.','files',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(18,'files.create','Create Files','Allow creating new files.','files',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(19,'files.delete','Delete Files','Allow deleting ALL files.','files',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(20,'users.view','View Users','Allow viewing ALL users.','users',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(21,'users.create','Create Users','Allow creating new users.','users',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(22,'users.update','Update Users','Allow updating ALL users.','users',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(23,'users.delete','Delete Users','Allow deleting ALL users.','users',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(24,'localizations.view','View Localizations','Allow viewing ALL localizations.','localizations',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(25,'localizations.create','Create Localizations','Allow creating new localizations.','localizations',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(26,'localizations.update','Update Localizations','Allow updating ALL localizations.','localizations',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(27,'localizations.delete','Delete Localizations','Allow deleting ALL localizations.','localizations',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(28,'mail_templates.view','View Mail Templates','Allow viewing ALL mail templates.','mail_templates',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(29,'mail_templates.update','Update Mail Templates','Allow updating ALL mail templates.','mail_templates',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(30,'settings.view','View Settings','Allow viewing ALL settings.','settings',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(31,'settings.update','Update Settings','Allow updating ALL settings.','settings',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(32,'plans.view','View Plans','Allow viewing ALL plans.','plans',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(33,'plans.create','Create Plans','Allow creating new plans.','plans',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(34,'plans.delete','Delete Plans','Allow deleting ALL plans.','plans',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(35,'artists.view','View Artists','Allow viewing ALL artists.','artists',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(36,'artists.create','Create Artists','Allow creating new artists.','artists',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(37,'artists.update','Update Artists','Allow updating ALL artists.','artists',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(38,'artists.delete','Delete Artists','Allow deleting ALL artists.','artists',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(39,'albums.view','View Albums','Allow viewing ALL albums.','albums',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(40,'albums.create','Create Albums','Allow creating new albums.','albums',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(41,'albums.update','Update Albums','Allow updating ALL albums.','albums',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(42,'albums.delete','Delete Albums','Allow deleting ALL albums.','albums',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(43,'tracks.view','View Tracks','Allow viewing ALL tracks.','tracks',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(44,'tracks.create','Create Tracks','Allow creating new tracks.','tracks','[{\"name\":\"minutes\",\"type\":\"number\",\"description\":\"How many minutes in total user tracks are allowed to take up. Leave empty for unlimited.\"}]','2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(45,'tracks.update','Update Tracks','Allow updating ALL tracks.','tracks',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(46,'tracks.delete','Delete Tracks','Allow deleting ALL tracks.','tracks',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(47,'tracks.download','Download Tracks','Allow  tracks.','tracks',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(48,'genres.view','View Genres','Allow viewing ALL genres.','genres',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(49,'genres.create','Create Genres','Allow creating new genres.','genres',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(50,'genres.update','Update Genres','Allow updating ALL genres.','genres',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(51,'genres.delete','Delete Genres','Allow deleting ALL genres.','genres',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(52,'lyrics.view','View Lyrics','Allow viewing ALL lyrics.','lyrics',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(53,'lyrics.create','Create Lyrics','Allow creating new lyrics.','lyrics',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(54,'lyrics.update','Update Lyrics','Allow updating ALL lyrics.','lyrics',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(55,'lyrics.delete','Delete Lyrics','Allow deleting ALL lyrics.','lyrics',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(56,'playlists.view','View Playlists','Allow viewing ALL playlists.','playlists',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(57,'playlists.create','Create Playlists','Allow creating new playlists.','playlists',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(58,'playlists.update','Update Playlists','Allow updating ALL playlists.','playlists',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(59,'playlists.delete','Delete Playlists','Allow deleting ALL playlists.','playlists',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(60,'channels.view','View Channels','Allow viewing ALL channels.','channels',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(61,'channels.create','Create Channels','Allow creating new channels.','channels',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(62,'channels.update','Update Channels','Allow updating ALL channels.','channels',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(63,'channels.delete','Delete Channels','Allow deleting ALL channels.','channels',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(64,'comments.view','View Comments','Allow viewing ALL comments.','comments',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(65,'comments.create','Create Comments','Allow creating new comments.','comments',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(66,'comments.update','Update Comments','Allow updating ALL comments.','comments',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(67,'comments.delete','Delete Comments','Allow deleting ALL comments.','comments',NULL,'2020-02-06 16:43:28','2020-02-06 16:43:28');

/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table playlist_track
# ------------------------------------------------------------

DROP TABLE IF EXISTS `playlist_track`;

CREATE TABLE `playlist_track` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `playlist_id` int(10) unsigned NOT NULL,
  `track_id` int(10) unsigned NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `playlist_track_track_id_playlist_id_unique` (`track_id`,`playlist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `playlist_track` WRITE;
/*!40000 ALTER TABLE `playlist_track` DISABLE KEYS */;

INSERT INTO `playlist_track` (`id`, `playlist_id`, `track_id`, `position`)
VALUES
	(1,1,1,1);

/*!40000 ALTER TABLE `playlist_track` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table playlist_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `playlist_user`;

CREATE TABLE `playlist_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `playlist_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `owner` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `playlist_user_playlist_id_user_id_unique` (`playlist_id`,`user_id`),
  KEY `playlist_user_owner_index` (`owner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `playlist_user` WRITE;
/*!40000 ALTER TABLE `playlist_user` DISABLE KEYS */;

INSERT INTO `playlist_user` (`id`, `playlist_id`, `user_id`, `owner`)
VALUES
	(1,1,2,1);

/*!40000 ALTER TABLE `playlist_user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table playlists
# ------------------------------------------------------------

DROP TABLE IF EXISTS `playlists`;

CREATE TABLE `playlists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `playlists_public_index` (`public`),
  KEY `playlists_views_index` (`views`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `playlists` WRITE;
/*!40000 ALTER TABLE `playlists` DISABLE KEYS */;

INSERT INTO `playlists` (`id`, `name`, `public`, `created_at`, `updated_at`, `image`, `description`, `views`)
VALUES
	(1,'test',1,'2020-02-07 08:27:34','2020-02-09 15:48:16','http://127.0.0.1:8000/client/assets/images/default/artist_small.jpg',NULL,2);

/*!40000 ALTER TABLE `playlists` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table reposts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reposts`;

CREATE TABLE `reposts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `repostable_id` int(11) NOT NULL,
  `repostable_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `reposts_repostable_id_repostable_type_user_id_unique` (`repostable_id`,`repostable_type`,`user_id`),
  KEY `reposts_repostable_id_index` (`repostable_id`),
  KEY `reposts_repostable_type_index` (`repostable_type`),
  KEY `reposts_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `legacy_permissions` text COLLATE utf8mb4_unicode_ci,
  `default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `guests` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `groups_name_unique` (`name`),
  KEY `groups_default_index` (`default`),
  KEY `groups_guests_index` (`guests`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `legacy_permissions`, `default`, `guests`, `created_at`, `updated_at`)
VALUES
	(1,'guests',NULL,0,1,'2020-02-06 16:43:28','2020-02-06 16:43:28'),
	(2,'users',NULL,1,0,'2020-02-06 16:43:28','2020-02-06 16:43:28');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `private` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_name_unique` (`name`),
  KEY `settings_private_index` (`private`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`id`, `name`, `value`, `created_at`, `updated_at`, `private`)
VALUES
	(1,'dates.format','yyyy-MM-dd','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(2,'dates.locale','en_US','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(3,'social.google.enable','true','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(4,'social.twitter.enable','true','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(5,'social.facebook.enable','true','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(6,'realtime.enable','false','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(7,'registration.disable','false','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(8,'branding.favicon','favicon.ico','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(9,'branding.logo_dark','client/assets/images/logo-dark.png','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(10,'branding.logo_light','client/assets/images/logo-light.png','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(11,'i18n.default_localization','english','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(12,'i18n.enable','true','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(13,'logging.sentry_public','','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(14,'realtime.pusher_key','','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(15,'themes.default_mode','light','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(16,'themes.user_change','true','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(17,'billing.enable','false','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(18,'billing.force_subscription','false','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(19,'billing.paypal_test_mode','true','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(20,'billing.stripe_test_mode','true','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(21,'billing.stripe.enable','false','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(22,'billing.paypal.enable','false','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(23,'billing.accepted_cards','[\"visa\",\"mastercard\",\"american-express\",\"discover\"]','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(24,'custom_domains.default_host','','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(25,'homepage.type','Channels','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(26,'homepage.value','5','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(27,'cache.report_minutes','60','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(28,'cache.homepage_days','1','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(29,'automation.artist_interval','7','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(30,'artist_provider','local','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(31,'album_provider','local','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(32,'radio_provider','spotify','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(33,'genres_provider','local','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(34,'search_provider','local','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(35,'audio_search_provider','local','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(36,'artist_bio_provider','wikipedia','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(37,'wikipedia_language','en','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(38,'genre_artists_provider','local','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(39,'providers.lyrics','lyricswikia','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(40,'youtube.suggested_quality','default','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(41,'youtube.region_code','US','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(42,'youtube.store_id','true','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(43,'player.default_volume','30','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(44,'player.hide_queue','0','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(45,'player.hide_video','0','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(46,'player.hide_video_button','0','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(47,'player.hide_lyrics','0','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(48,'player.mobile.auto_open_overlay','1','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(49,'player.enable_download','0','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(50,'player.sort_method','external','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(51,'player.artist_type','artist','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(52,'player.seekbar_type','line','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(53,'player.track_comments','false','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(54,'player.show_upload_btn','false','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(55,'player.default_artist_view','list','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(56,'https.enable_cert_verification','1','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(57,'site.force_https','0','2020-02-06 16:43:28','2020-02-06 16:43:28',0),
	(58,'menus','[{\"name\":\"Primary\",\"position\":\"sidebar-primary\",\"items\":[{\"type\":\"route\",\"order\":1,\"label\":\"Popular Albums\",\"action\":\"\\/popular-albums\",\"icon\":\"album\"},{\"type\":\"route\",\"order\":2,\"label\":\"Genres\",\"action\":\"\\/genres\",\"icon\":\"local-offer\"},{\"type\":\"route\",\"order\":3,\"label\":\"Popular Tracks\",\"action\":\"\\/popular-tracks\",\"icon\":\"trending-up\"},{\"type\":\"route\",\"order\":4,\"label\":\"New Releases\",\"action\":\"\\/new-releases\",\"icon\":\"new-releases\"}]},{\"name\":\"Secondary \",\"position\":\"sidebar-secondary\",\"items\":[{\"type\":\"route\",\"order\":1,\"label\":\"Songs\",\"action\":\"\\/library\\/songs\",\"icon\":\"audiotrack\"},{\"type\":\"route\",\"order\":2,\"label\":\"Albums\",\"action\":\"\\/library\\/albums\",\"icon\":\"album\"},{\"type\":\"route\",\"order\":3,\"label\":\"Artists\",\"action\":\"\\/library\\/artists\",\"icon\":\"mic\"},{\"type\":\"route\",\"order\":3,\"label\":\"History\",\"action\":\"\\/library\\/history\",\"icon\":\"history\"}]},{\"name\":\"Mobile \",\"position\":\"mobile-bottom\",\"items\":[{\"type\":\"route\",\"order\":1,\"label\":\"Genres\",\"action\":\"\\/genres\",\"icon\":\"local-offer\"},{\"type\":\"route\",\"order\":2,\"label\":\"Top 50\",\"action\":\"\\/popular-tracks\",\"icon\":\"trending-up\"},{\"type\":\"route\",\"order\":3,\"label\":\"Search\",\"action\":\"\\/search\",\"icon\":\"search\"},{\"type\":\"route\",\"order\":4,\"label\":\"Your Music\",\"action\":\"\\/library\",\"icon\":\"library-music\"},{\"type\":\"route\",\"order\":4,\"label\":\"Account\",\"action\":\"\\/account\\/settings\",\"icon\":\"person\"}]}]','2020-02-06 16:43:28','2020-02-06 18:48:14',0),
	(59,'homepage.appearance','{\"headerTitle\":\"Connect on BeMusic\",\"headerSubtitle\":\"Discover, stream, and share a constantly expanding mix of music from emerging and major artists around the world.\",\"headerImage\":\"client\\/assets\\/images\\/landing\\/landing-header-bg.jpg\",\"headerOverlayColor1\":\"rgba(16,119,34,0.56)\",\"headerOverlayColor2\":\"rgba(42,148,71,1)\",\"footerTitle\":\"Make music? Create audio?\",\"footerSubtitle\":\"Get on BeMusic to help you connect with fans and grow your audience.\",\"footerImage\":\"client\\/assets\\/images\\/landing\\/landing-footer-bg.jpg\",\"actions\":{\"inputText\":\"Search for artists, albums and tracks...\",\"inputButton\":\"Search\",\"cta1\":\"Signup Now\",\"cta2\":\"Explore\"},\"primaryFeatures\":[],\"secondaryFeatures\":[{\"title\":\"Watch Anytime, Anywhere. From Any Device.\",\"subtitle\":\"Complete Freedom\",\"image\":\"client\\/assets\\/images\\/landing\\/landing-feature-1.jpg\",\"description\":\"Stream music in the browser, on Phone, Tablet, Smart TVs, Consoles, Chromecast, Apple TV and more.\"},{\"title\":\"Get More From Bemusic With Pro\",\"subtitle\":\"BeMusic Pro\",\"image\":\"client\\/assets\\/images\\/landing\\/landing-feature-2.jpg\",\"description\":\"Subscribe to BeMusic pro to hide ads, increase upload time and get access to other exclusive features.\"}],\"channelIds\":[1]}','2020-02-06 16:43:28','2020-02-06 16:43:28',0);

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table similar_artists
# ------------------------------------------------------------

DROP TABLE IF EXISTS `similar_artists`;

CREATE TABLE `similar_artists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `artist_id` int(10) unsigned NOT NULL,
  `similar_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `similar_artists_artist_id_similar_id_unique` (`artist_id`,`similar_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table sitemap_ids
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sitemap_ids`;

CREATE TABLE `sitemap_ids` (
  `id` int(10) unsigned NOT NULL,
  KEY `sitemap_ids_id_index` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table social_profiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `social_profiles`;

CREATE TABLE `social_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `service_name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_service_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `social_profiles_user_id_service_name_unique` (`user_id`,`service_name`),
  UNIQUE KEY `social_profiles_service_name_user_service_id_unique` (`service_name`,`user_service_id`),
  KEY `social_profiles_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table subscriptions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subscriptions`;

CREATE TABLE `subscriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `plan_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gateway` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'none',
  `gateway_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'none',
  `quantity` int(11) NOT NULL DEFAULT '1',
  `description` text COLLATE utf8mb4_unicode_ci,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `renews_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subscriptions_gateway_index` (`gateway`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table taggables
# ------------------------------------------------------------

DROP TABLE IF EXISTS `taggables`;

CREATE TABLE `taggables` (
  `tag_id` int(10) unsigned NOT NULL,
  `taggable_id` int(10) unsigned NOT NULL,
  `taggable_type` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  UNIQUE KEY `taggables_tag_id_taggable_id_user_id_taggable_type_unique` (`tag_id`,`taggable_id`,`user_id`,`taggable_type`),
  KEY `taggables_tag_id_index` (`tag_id`),
  KEY `taggables_taggable_id_index` (`taggable_id`),
  KEY `taggables_taggable_type_index` (`taggable_type`),
  KEY `taggables_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'custom',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tags_name_type_unique` (`name`,`type`),
  KEY `tags_type_index` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table track_plays
# ------------------------------------------------------------

DROP TABLE IF EXISTS `track_plays`;

CREATE TABLE `track_plays` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `track_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `platform` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `browser` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `track_plays_user_id_index` (`user_id`),
  KEY `track_plays_track_id_index` (`track_id`),
  KEY `track_plays_platform_index` (`platform`),
  KEY `track_plays_device_index` (`device`),
  KEY `track_plays_browser_index` (`browser`),
  KEY `track_plays_location_index` (`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `track_plays` WRITE;
/*!40000 ALTER TABLE `track_plays` DISABLE KEYS */;

INSERT INTO `track_plays` (`id`, `user_id`, `track_id`, `created_at`, `platform`, `device`, `browser`, `location`)
VALUES
	(1,2,1,'2020-02-07 08:28:14','os x','desktop','chrome','us');

/*!40000 ALTER TABLE `track_plays` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tracks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tracks`;

CREATE TABLE `tracks` (
  `id` int(11) NOT NULL,
  `md_code` text,
  `name` text,
  `library` text,
  `album_name` text,
  `track_no` int(11) DEFAULT NULL,
  `length` text,
  `version` text,
  `bitrate` int(11) DEFAULT NULL,
  `frequency` text,
  `genre` text,
  `bpm` int(11) DEFAULT NULL,
  `tempo` text,
  `keyword` text,
  `instruments` text,
  `url` text,
  `album_id` text,
  `number` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `artists_legacy` text,
  `youtube_id` text,
  `spotify_popularity` text,
  `temp_id` int(11) DEFAULT NULL,
  `plays` int(11) DEFAULT NULL,
  `auto_update` int(11) DEFAULT NULL,
  `local_only` int(11) DEFAULT NULL,
  `spotify_id` text,
  `created_at` text,
  `updated_at` text,
  `user_id` text,
  `description` text,
  `image` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tracks` WRITE;
/*!40000 ALTER TABLE `tracks` DISABLE KEYS */;

INSERT INTO `tracks` (`id`, `md_code`, `name`, `library`, `album_name`, `track_no`, `length`, `version`, `bitrate`, `frequency`, `genre`, `bpm`, `tempo`, `keyword`, `instruments`, `url`, `album_id`, `number`, `duration`, `artists_legacy`, `youtube_id`, `spotify_popularity`, `temp_id`, `plays`, `auto_update`, `local_only`, `spotify_id`, `created_at`, `updated_at`, `user_id`, `description`, `image`)
VALUES
	(1,'DWDWB000100000100','Left Bank Two','De Wolfe Web','UK TV THEME TUNES',1,'0:02:28','',NULL,'','Lounge',116,'Mid Tempo','Well Known Tunes,TV Theme Tunes,Vision On,The Gallery,Easy','Instrumental,Vibraphone,Tuned Percussion,Small Group','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DEWEB0001_001_Left+Bank+Two.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(2,'DWDWB001000000100','Great Leaders','De Wolfe Web','POLITICAL WORLD',1,'0:04:10','',NULL,'','',90,'Mid Tempo','Positive,America,Campaign,Celebrate,Heroic,Warm,Patriotic,North America,Elections,Politics,Pro-Candidate,Rah Rah','Orchestra,Instrumental','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DEWEB0010_001_Great+Leaders.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(3,'DWDWB002400000100','Spirit Of Iran','De Wolfe Web','IRAN',1,'0:04:22','',NULL,'','Ethnic',160,'Mid Tempo','Rhythmic,Solid,Iran','Instrumental','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DEWEB0024_001_Spirit+Of+Iran.MP3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(4,'DWDWC000100000100','Windjammer','De Wolfe Music','MIRAGE',1,'0:03:56','',NULL,'','',146,'Up Tempo','Driving,Corporate,Sport,1980s,Achievement,Exuberant,Pacy','Electronic,Instrumental','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DWCD0001_001_Windjammer.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(5,'DWDWC001800000100','Musical Portrait 1','De Wolfe Music','CLASSICS ONE',1,'0:03:26','',NULL,'','Classical,Historical',125,'Up Tempo','Stately,Light,Classical Era,Confident,Melodic,Period','Orchestra,Strings,Instrumental','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DWCD0018_001_Musical+Portrait+1.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(6,'DWDWC002200200300','Shapes Of Hamsa','De Wolfe Music','SWITCHCRAFT',3,'0:03:06','',NULL,'','Drama',150,'Up Tempo','Sport,1980s,Action,Aggressive,Angry,Tough,Powerful,Mighty','Electronic,Instrumental,Orchestral Stab','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DWCD0022_003_Shapes+Of+Hamsa.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(7,'DWDWM000100000100','For Quality - Vox','Commercial Breaks','BEDS WITH VOCALS',1,'0:01:00','',NULL,'','Song',120,'Mid Tempo','Lively,Positive,Fashion,Groove,Catwalk,Punchy,Jingles,1.00,Emphatic,Refreshing','Female Vocal,Vocals,Small Group,Voice','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DWCOM0001_001_For+Quality_Vox.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(8,'DWDWM000800000100','Rock Star','Commercial Breaks','ROCK, BLUES AND COUNTRY',1,'0:01:00','',NULL,'','Rock',90,'Mid Tempo','Positive,America,Jingles,Jaunty','Electric Guitar,Instrumental,Small Group,Guitar,Guitars','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DWCOM0008_001_Rock+Star.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(9,'DWDWM002100000100','Swinger','Commercial Breaks','JAZZ',1,'0:01:00','',NULL,'','Jazz,Cool Jazz',120,'Up Tempo','Positive,Cool,Confident,Jingles,1.00,Latin American','Piano,Instrumental,Brass,Small Group','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DWCOM0021_001_Swinger.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(10,'DWDWJ000100000100','The Strutter\'s Walk','De Wolfe Jazz','THE JAZZ YEARS 1900s & 1910s',1,'0:02:38','',NULL,'','Historical,Archive,Jazz,Cakewalk',98,'Mid Tempo','1900s,America,Jaunty,Period','Instrumental,Small Group','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DWJAZZ0001_001_The+Strutter\'s+Walk.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(11,'DWDWJ000400100200','Fool\'s Gold','De Wolfe Jazz','THE JAZZ YEARS 1960s & 1970s',2,'0:03:10','',NULL,'','Historical,Jazz',136,'Mid Tempo','1960s,Catchy,Soulful,Period,Easy','Piano,Flute,Instrumental,Large Group','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DWJAZZ0004_002_Fool\'s+Gold.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(12,'DWDWJ000700200300','Calling All Girls','De Wolfe Jazz','THE JAZZ YEARS - JAZZ PIANO PART 1 - 1900-1950S',3,'0:03:23','',NULL,'','Historical,Jazz',123,'Up Tempo','1920s,1910s,Cheeky,Playful,Period','Piano,Solo Piano,Instrumental,Stride Piano,Solo,Jazz Piano','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DWJAZZ0007_003_Calling+All+Girls.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(13,'DWDWL279700200300','Shimmering Sands','De Wolfe Vinyl','MOOD MEDITERRANEAN',3,'0:04:00','',NULL,'','Archive,60s TV',78,'Slow','Leisure,1960s,Exotic,Swaying','Instrumental,Mandolin','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DWLP2797_003_Shimmering+Sands.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(14,'DWDWL282400600700','Sugar And Spice','De Wolfe Vinyl','YOUNG WORLD',7,'0:02:51','',NULL,'','Rock,Surf Rock,60s Rock',132,'Mid Tempo','Romantic,Gentle,Leisure,Holidays,1960s,Surf,Tuneful','Electric Guitar,Saxophone,Tenor Saxophone,Guitar','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DWLP2824_007_Sugar+And+Spice.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(15,'DWDWL314600200300','Policeman\'s Holiday','De Wolfe Vinyl','SHEP\'S A GO-GO',3,'0:03:29','',NULL,'','Comedy Music,Novelty',122,'Mid Tempo','1960s,Humorous,Jaunty,Period','Piano,Instrumental,Banjo,Trombone,Sousaphone','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DWLP3146_003_Policeman\'s+Holiday.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(16,'DWDWI000100500600','Victorian & Edwardian Moments','De Wolfe 20th Century Archive','VICTORIAN AND EDWARDIAN MOMENTS',6,'0:02:51','',NULL,'','Historical',71,'Slow','Fantasy,Romantic,Meditation,1900s,Reflective,Yearning,Loving,Love,Edwardian,Victorian,Emotional,Emotive,Period,Pondering','Piano,Instrumental,Quartet','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DWMIL0001_006_Victorian+%26+Edwardian+Moments.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(17,'DWDWI000700700800','Shadows On The Wall','De Wolfe 20th Century Archive','1930s CINEMA',8,'0:03:40','',NULL,'','Historical,Drama',67,'Slow','1930s,Fear,Chilled,Cinema,Spooky,Nightmare,Film,Unsettling,Suspicion,Terror,Terrifying,Hollywood,Newsreel,Startling,Spine-chilling,Traumatic,Bloodcurdling,Frightened,Hesitant,Period,Shocking','Orchestra,Instrumental,Large Orchestra','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DWMIL0007_008_Shadows+On+The+Wall.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(18,'DWDWI001000200300','Party Mood','De Wolfe 20th Century Archive','1940s POPULAR STYLES/CINEMA 3',3,'0:03:16','',NULL,'','Historical,Big Band,Jazz,Swing',98,'Up Tempo','1940s,America,Period','Instrumental','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DWMIL0010_003_Party+Mood.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(19,'DWDWR000300000100','Suite Bergamasque - Clair De Lune','RPO Classical Moods','ATMOSPHERIC - CLASSICAL MOODS',1,'0:05:42','',NULL,'','Classical',89,'Mid Tempo','Gentle,Reflective,Claire De Lune','Solo Piano,Instrumental','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DWRPO0003_001_Suite+Bergamasque+-+Clair+De+Lune.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(20,'DWDWR000900000100','The Music For The Royal Fireworks La Paix','RPO Classical Moods','PASTORAL - CLASSICAL MOODS',1,'0:03:03','',NULL,'','Classical,Pastoral',108,'Mid Tempo','Majestic,Peaceful,Regal,Refined,Restful','Orchestra,Instrumental','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DWRPO0009_001_The+Music+For+The+Royal+Fireworks+La+Paix.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(21,'DWDWR002800000900','The Music For The Royal Fireworks - Overture','RPO Classical Moods','BAROQUE ERA 2 - CLASSICAL MOODS',9,'0:08:23','',NULL,'','Classical,Baroque',135,'Mid Tempo','Stately,Proud','Orchestra,Instrumental','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DWRPO0028_009_The+Music+For+The+Royal+Fireworks+-+Overture.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(22,'DWDWU000200000100','U.S. Gold','Solid State Music','BUSINESS, NEWS AND SPORT',1,'0:03:03','',NULL,'','',117,'Up Tempo','Light,Corporate,Achievement,Celebrate,Flowing,Prestige,Glamour,Glamorous,Outstanding','Instrumental,Large Group','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DWUSA0002_001_U.S.+Gold.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(23,'DWDWU001600000100','End Of The Day','Solid State Music','SMOOTH JAZZ',1,'0:03:05','',NULL,'','Easy Listening,Jazz',96,'Mid Tempo','Leisure,Lifestyle,Carefree,Smooth,Unhurried,Jaunty','Instrumental,Saxophone,Small Group','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DWUSA0016_001_End+Of+The+Day.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(24,'DWDWU003600000100','Country Lanes','Solid State Music','LIFE IS GOOD',1,'0:03:23','',NULL,'','Folk',115,'Up Tempo','Light,Countryside,Leisure,Gardens,Outdoor,Rural,Rolling','Instrumental,Ukulele,Small Group,Folk Group','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/DWUSA0036_001_Country+Lanes.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(25,'DWHMC050400300400','The Glass Triangle','Hudson Music Library','CLASSICAL COLOURS',4,'0:03:38','',NULL,'','Easy Listening,Lounge',162,'Mid Tempo','Romantic,Leisure,1960s,Lilting,Sentimental,Testcard,Fresh,Period','Orchestra,Strings,Instrumental,Harpsichord','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/HMCLP0504_004_The+Glass+Triangle.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(26,'DWHMW000100300400','Incredible','Hudson Web','STYLES 5',4,'0:03:24','',NULL,'','',150,'Mid Tempo','Uplifting,Positive,Sport,Achievement,Building,Confident,Rocking,Revolving,Success','Instrumental,Small Group,Rock Band','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/HMWEB0001_004_Incredible.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(27,'DWHMW000200000100','Jerusalem','Hudson Web','NOVELTY TRACKS',1,'0:01:01','',NULL,'','Reggae',73,'Mid Tempo','England,Quirky,Rugby,Hymn,Jerusalem,Well Known Tunes,UK / United Kingdom','Instrumental','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/HMWEB0002_001_Jerusalem.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(28,'DWHMW000300100200','Safe House','Hudson Web','SPYMASTER',2,'0:02:20','',NULL,'','Drama',83,'','Dark,Crime,Intrigue,Unsettling,Spy,Thriller,Undercover,Tension Builder,Stealthy,Spies,Filmic,Subversive,Detective,Hesitant,Film  And TV Styles,Espionage,Surveillance','Orchestra,Instrumental,Large Orchestra','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/HMWEB0003_002_Safe+House.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(29,'DWHRC000100901000','Mind, Body And Soul','Hudson Music Library','MIND, BODY AND SOUL',10,'0:03:36','',NULL,'','Rock,80s Pop',108,'Mid Tempo','Optimistic,1980s,Confident','Instrumental,Small Group','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/HRCB0001_001_Mind%2C+Body+%26+Soul_Vocal.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(30,'DWHRC001400000100','Touch of Warmth','Hudson Music Library','STYLES 2',1,'0:02:38','',NULL,'','Song',115,'Mid Tempo','Fantasy,Romantic,1960s,Carefree,Loving,Love,Jaunty,Film  And TV Styles','Orchestra,Female Vocal,Vocals,Voice','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/HRCB0014_001_Touch+of+Warmth.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(31,'DWHRC003600000100','To The Top','Hudson Music Library','ROCK SOLID',1,'0:03:03','',NULL,'','Rock,Alternative,Alternative Rock,Emo',130,'Mid Tempo','Driving,Sport,2000s,Confident','Instrumental,Rock Band','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/HRCB0036_001_To+The+Top.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(32,'DWRMC200200000100','L.A. Dreaming','Rouge Music Library','SKETCHES',1,'0:02:13','',NULL,'','Easy Listening,Duet',113,'Mid Tempo','Romantic,Leisure,Lifestyle,1980s,Nostalgic,Cool,Reflective,Regret,Loving,Love,Pondering','Instrumental,Alto Saxophone,Saxophone,Small Group','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/RMCD2002_001_L.A.+Dreaming.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(33,'DWRMC201400100200','The Human Element','Rouge Music Library','THE MAIN OBJECTIVE',2,'0:03:44','',NULL,'','',91,'Mid Tempo','Technology,Corporate,Industry,1990s,Achievement,Hopeful,Confident,Proud,Elated,Jingles,Hi-tech,Industrial,Illustrious,News Theme','Electronic,Instrumental','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/RMCD2014_002_The+Human+Element.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(34,'DWRMC208700000100','A Hero Will Rise','Rouge Music Library','EPIC ADVENTURES',1,'0:04:19','',NULL,'','Film Scores',120,'Up Tempo','Fantasy,Action,Blockbuster,Cinema,Powerful,Heroic,Imposing,Grand,Epic,Hollywood,Percussive,Mighty,Filmic,Tribal,Pulsating,Forceful','Orchestra,Instrumental','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/RMCD2087_001_A+Hero+Will+Rise.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(35,'DWRMS010100000100','Summer Vibes','Rouge Vinyl','SKYBOAT',1,'0:03:24','',NULL,'','60s Pop,Surf Pop,80s Pop',66,'Mid Tempo','Leisure,Lifestyle,Holidays,1960s,America,Carefree,Surf','Unworded Vocals,Large Group','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/RMSLP0101_001_Summer+Vibes.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(36,'DWRMS011100200300','Empty Plain','Rouge Vinyl','KITES',3,'0:02:45','',NULL,'','Drone,Ambient',137,'Mid Tempo','Dark,Space,Sparse,1970s,Cold,Spacey,Linear','Electronic,Instrumental,Synth,Small Group','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/RMSLP0111_003_Empty+Plain.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(37,'DWRMS013700000100','Colosseum','Rouge Vinyl','VOYAGE OF DISCOVERY',1,'0:03:47','',NULL,'','Classical,Historical',97,'Mid Tempo','Aggressive,Battle,Bold,Building,Imposing,Grand,War,Epic,Roman Epic,Roman,Martial','Orchestra,Instrumental,Fanfare Opening','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/RMSLP0137_001_Colosseum.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(38,'DWRMW000100000100','Fearless','Rouge Web','WAR ON TERROR 2',1,'0:03:14','',NULL,'','',85,'Up Tempo','Driving,Crime,Aggressive,Chilling,Dynamic,Powerful,War,Terror,Terrorism,Rocky,Pulsating,Forceful','Electronic,Instrumental,Small Group','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/RMWEB0001_001_Fearless.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(39,'DWRMW000300000100','Barefoot','Rouge Web','CALIFORNIA JAZZ',1,'0:04:36','',NULL,'','Breakbeat,Jazz,West Coast Jazz,West Coast',108,'Up Tempo','America,Carefree,Sophisticated,California','Instrumental,Small Group','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/RMWEB0003_001_Barefoot.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(40,'DWRMW000400200300','Terror Zone','Rouge Web','DISASTERS,CRIME AND INVESTIGATION',3,'0:03:28','',NULL,'','Drone',72,'Slow','Crime,Evil,Terror,Terrorism,Rolling,Pushy,Murder','Electronic,Instrumental,Small Group','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/RMWEB0004_003_Terror+Zone.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(41,'DWSMC050500100200','Zenith','Sylvester Music','ZENITH',2,'0:02:32','',NULL,'','Drama',97,'Mid Tempo','Dark,Science,Space,1950s,1960s,Documentary,Desolate,Sci-Fi,Drifting,Slow Moving','Orchestra,Instrumental','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/SMCLP0505_002_Zenith.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(42,'DWSMC050700200300','Go','Sylvester Music','GO',3,'0:02:43','',NULL,'','Pop,Euro Pop',156,'Mid Tempo','Positive,Rhythmic,1960s,Active,Fast Moving,Gallop','Organ,Electric Guitar,Guitar','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/SMCLP0507_003_Go.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(43,'DWSMC055700000100','Bright And Shining','Sylvester Music','BRIGHT AND SHINING',1,'0:02:25','',NULL,'','Easy Listening,Song',118,'Mid Tempo','Positive,Leisure,Lifestyle,1980s,Sophisticated,Jazzy','Vocals,Saxophone,Small Group,Vocal Group','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/SMCLP0557_001_Bright+And+Shining.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(44,'DWSTC000100000100','Music to Rosamunde, Op.26 - V. Ballet Music No.2 - Andantino','Synctracks Classical','Synctracks Classical Collection: Adventure & Exploration',1,'0:07:04','',NULL,'','Classical,Drama',0,'Mid Tempo','19th Century,Ambling,Austria,Lilting,Confident','Orchestra,Instrumental,Fiddle','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/STCC0001_001_Music+to+Rosamunde%2C+Op.26+-+V.+Ballet+Music+No.2+-+Andantino.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(45,'DWSTC001500800900','Ruslan and Lyudmila - Oriental Dances from the 4th Act - I. Turkish - Allegretto','Synctracks Classical','Synctracks Classical Collection: Dramatic & Passionate',9,'0:02:45','',NULL,'','Classical,Baroque,Drama',0,'Slow','19th Century,Romantic,Bold,Longing,Reflective,Film,Yearning,Passionate,Tragic,Sentimental,Retrospective,Russia,Imploring','Orchestra,Instrumental','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/STCC0015_009_Ruslan+and+Lyudmila+-+Oriental+Dances+from+the+4th+Act+-+I.+Turkish+-+Allegretto.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(46,'DWSTC004900000100','Polka Op 51 No3','Synctracks Classical','Synctracks Classical Collection: Serious & Sombre',1,'0:04:54','',NULL,'','Classical,Waltz,Drama',0,'Mid Tempo','19th Century,Serious,Elegant,Film,Lilting,Charming,Russia','Piano,Solo Piano,Instrumental','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/STCC0049_001_Polka+Op+51+No3.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(47,'DWSYN000100000100','Journeying','Synctracks Music','Acoustic Feelgood',1,'0:02:40','',NULL,'','Country Music,Jig',134,'Up Tempo','Lively,Uplifting,Daytime TV,Optimistic,Countryside,Holidays,Ambling,Bustling,Catchy,Summer,Playful,Make-Overs,Inspiring,Delight','Acoustic Guitar,Instrumental,Banjo,Bass','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/SYNC0001_001_Journeying.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(48,'DWWID000100100200','Darkness Rising','Widescreen','IMPACT TRAILERS',2,'0:02:41','',NULL,'','Sound Design,Drama,Film Scores',96,'Mid Tempo','Action,Building,Cinematic,Film,Powerful,Epic,Percussive,Hard Hitting,Filmic,Action Packed,Trailers,Impacts','Orchestra,Electric Guitar,Instrumental,Drums,Horns,Guitar,Large Orchestra','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/WIDE0001_002_Darkness+Rising.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(49,'DWWID000300000100','Below Zero','Widescreen','SOUND DESIGN 1',1,'0:03:32','',NULL,'','Sound Design,Drama,Film Scores',80,'Slow','Science,Technology,Blockbuster,Cinema,Cinematic,Film,Sci-Fi,Tension Builder,Filmic,Modern Drama,Trailers,Nordic Noir','Electronic,Strings,Percussion,Instrumental','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/WIDE0003_001_Below+Zero.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(50,'DWWID000600000100','Hang By A Thread','Widescreen','SINISTER DRAMA',1,'0:02:59','',NULL,'','Sound Design,Drama',95,'Slow','Pulsing,Building,Building To End,Trailers,Urban Drama,Nordic Noir','Strings,Instrumental,Bass','https://tempd.s3.ap-northeast-2.amazonaws.com/DE_WOLFE_DATA/WIDE0006_001_Hang+By+A+Thread.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(51,'EXAT000100100','Scarborough Fair','A-Tone','ATN001 All The Pretty Horses',1,'03:09','Full Version',24,'48','Folk',93,'Medium','Creeping, Melancholy, Low, Ominous, Mysterious, Anticipation, Stealth, Air, Distorted, Contemplative','Acoustic Guitar, Strings','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/ATN0001_001_A_Scarborough+Fair_Full.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(52,'EXDI000200100','Victory Quest','Directors Cuts','DCD002 Action',1,'01:20','Full Version',24,'48','Score',110,'Medium','Heroic, Motivational, Victory, Epic, Dynamic, Serious, Trailers, Thematic, Panoramic, Football, Boxing, Memorial, Independence Day, Orchestral, Struggle, Fight, Glory','Strings, Snare Drum','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/DCD0002_001_A_Victory+Quest.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(53,'EXLR000100100','Heaven Bound','Qseries','QCD001 Gospel',1,'02:19','Full Version',24,'48','Religious',117,'Medium','Christian, Funky, Motivational, Driving, Lively, Powerful, Soulful','Electric Piano','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/QCD0001_001_A_Heaven+Bound_Full+Version.MP3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(54,'EXMX000100100','Earth Song','Xseries','XCD001 Creation',1,'01:00','Full Version',24,'48','Electronica',122,'Medium - Fast','Africa, Surreal, Tribal, Discovery, Electronic, Intrigue, Rainforest, Bouncy, Bright','Piano, Fx','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XCD0001_001_Earth+Song_Full+Version.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(55,'EXUX010100100','Somebody2love','Mixtape','XMT101 Dance Pop',1,'02:16','Full Version',24,'48','Pop',130,'Medium','Fashion, Pulse, Ibiza, Anthemic, Electronic, Party, Driving, Song, Energetic, Bright, Dance, Catchy','Synthesizer','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XMT0101_001_A_Somebody2Love_Full.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(56,'EXAT000900100','Hush Puppy\'s Theme','A-Tone','ATN009 Airtight Alibi',1,'02:57','Full Version',24,'48','R&b',120,'Medium - Fast','Detective, Groove, Cool, Flowing, Slick, Smooth, Bouncy, Bright, Anticipation, Dance, Comedy','Brass, Electric Guitar, Flute, Bongos','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/ATN0009_001_A_Hush+Puppy%E2%80%99S+Theme_Full.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(57,'EXAT001200400','O Come All Ye Faithful','A-Tone','ATN012 The Naughty List',4,'03:16','Full Version',24,'48','Holiday',117,'Medium','Winter, Light, Bouncy, Sweet, Builds, Slow Dance, Swells','Brass, Piano, Saxophone','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/ATN0012_004_A_O+Come+All+Ye+Faithful_Full.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(58,'EXGA001100800','Kill Room (Part 1)','Law & Audio','LAA011 Crime Scenes 2',8,'03:11','Full Version',24,'48','Score',79,'Slow','Clocks, Danger, Detective, Floating, Mischievous, Thriller, Eerie, Scary, Electronic, Intrigue, Serious, Underscore, Suspense, Mysterious, Sinister, Anticipation, Terror, Music Box, Endings, Builds, Devious, Forensics','Strings','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/LAA0011_008_A_KILL+ROOM+PART+1.MP3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(59,'EXGM003600700','Lapse Of Judgement','Law & Audio','LAA036 Interventions',7,'03:36','Full Version',24,'48','Score',50,'Slow','Creeping, Light, Ambient, Melancholy, Underscore, Ominous, Mysterious, Anticipation, Air, Swells, Wistful, Contemplative','Electric Guitar','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/LAA036_007_Lapse+Of+Judgement_Main.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(60,'EXXS010200100','Where Is My Native Fellow','Passport','XPS102 Passport To Ethiopia',1,'03:13','Full Version',24,'48','World',120,'Medium','Africa, Repetitive, Tribal, Macho, Travel','African Drums','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XPS0102_001_Where+Is+My+Native+Fellow\'+(Vox).mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(61,'EXXS011400200','La Bamba','Passport','XPS114 Passport To Mexico',2,'03:02','Full Version',24,'48','World',120,'Medium','Latin, Party, Acoustic, Song, Travel, Energetic, Mariachi, Cinco De Mayo, Strum, Latin America, Stings','Trumpet, Nylon Guitar, Guitarron, Vihuela','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XPS0114_002_A_La+Bamba+(Vox).mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(62,'EXLA000501300','July Jam','Qseries','QCD005 Blues',13,'03:55','Full Version',24,'48','Blues',56,'Slow','Gentle, Smooth, Song, Laid Back, Solo, Jazz Trio, Jazzy','Jazz Guitar, Piano','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/QCD0005_013_July+Jam_Full+Version.MP3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(63,'EXLA000800700','Unfound Mind','Scoreganics','SCS008 Paranoia',7,'03:12','Full Version',24,'48','Score',NULL,'Variable','Clocks, Floating, Eerie, Electronic, Ambient, Shimmering','Synthesizer','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/SCS0008_007_A_Unfound+Mind_Full+Version.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(64,'EXMX000400200','Angels Falling Down','Saint Rogue','SRG004 Shadow Man',2,'03:52','Full Version',24,'48','Rock',160,'Medium','Creeping, Electronic, Serious, Gothic, Mysterious, Evil, Sinister, Anticipation, Builds, Struggle, Devious, Boomy, Contemplative','Strings','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/SRG0004_002_A_Angels+Falling+Down_Full+Version.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(65,'EXMX000400700','This World Is Not My Home','Saint Rogue','SRG004 Shadow Man',7,'03:30','Full Version',24,'48','Rock',62,'Slow - Medium','Creeping, Serious, Mysterious, Anticipation, Builds, Struggle, Devious, Contemplative','Electric Organ','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/SRG0004_007_A_This+World+Is+Not+My+Home_Full+Version.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(66,'EXMX000100500','Devastated','Velvet Ears','VEX001 Velvet Ears 1',5,'03:10','Full Version',24,'48','Rock',92,'Slow','Film Noir, Lonely, Gentle, Poignant, Song, Ethereal, Melancholy','Electric Guitar, Piano','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/VEX0001_005_A_DEVASTATED_Full.MP3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(67,'EXMX002000100','Drive Us Away','Velvet Ears','VEX020 Velvet Ears 20',1,'03:10','Full Version',24,'48','Pop',63,'Slow','Grief, Poignant, Song, Melancholy, Sparse, Loser, Barren','Drums','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/VEX0020_001_A_DRIVE+US+AWAY.MP3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(68,'EXMX000100300','Pluck Off','Velvet Ears','VNC001 Neo Classical 1',3,'03:27','Full Version',24,'48','Classical',NULL,'Variable','Mischievous, Quirky, Score, Surreal, Weird, Sneaky, Dissonant, Science, Bouncy, Anticipation, Pizzacato','Piano, Pizzicato Strings','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/VNC0001_003_A_PLUCK+OFF.MP3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(69,'EXMX000400800','Cats Cradle','Velvet Ears','VNC004 Neo Classical 4',8,'03:00','Full Version',24,'48','Classical',121,'Medium','Flowing, Thematic, Driving, Punchy, Bright, Builds, Orchestral, Contemplative','Piano, Strings','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/VNC0040_008_A_CATS+CRADLE.MP3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(70,'EXMX000300200','Pulse Check','Velvet Ears','VNX003 Time 1',2,'03:44','Full Version',24,'48','Classical',97,'Medium','Clocks, Repetitive, Discovery, Electronic, Science, Suspense, Neutral, Anticipation, Sparse, Barren','Piano, Fx','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/VNX0003_002_A_PULSE+CHECK.MP3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(71,'EXMX000301000','Wind Up','Velvet Ears','VNX003 Time 1',10,'03:03','Full Version',24,'48','Classical',110,'Medium','Clocks, Danger, Electronic, Suspense, Ominous, Evil, Anticipation, Terror','Percussion, Fx','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/VNX0003_010_A_WIND+UP.MP3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(72,'EXMX002100200','O Come All Ye Faithful','Velvet Ears','VNX021 Xmas 2',2,'03:29','Full Version',24,'48','Holiday',90,'Medium','Floating, Weird, Winter, Lonely, Discovery, Electronic, Melancholy, Space, Builds, Swells, Wistful, Boomy','Acoustic Guitar, Piano','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/VNX0021_002_A_O+Come+All+Ye+Faithful_Full.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(73,'EXMX000200400','Advance Party','War & Audio','WAA002 Ride Out',4,'03:42','Full Version',24,'48','Score',125,'Medium','Creeping, Pulse, War, Dynamic, Electronic, Serious, Suspense, Intense, Anticipation, Builds, Stealth','Percussion, Synthesizer','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/WAA0002_004_A_ADVANCE+PARTY.MP3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(74,'EXMX001100900','The Watch List','War & Audio','WAA011 Espionage',9,'03:15','Full Version',24,'48','Score',120,'Medium','Creeping, Electronic, Intrigue, Pursuit, Serious, Suspense, Anticipation, Contemplative','Strings','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/WAA011_009_The+Watch+List_Main.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(75,'EXMX000400500','The Best Laid Plans','Xseries','XCD004 Cheeze',5,'03:28','Full Version',24,'48','Score',88,'Medium','Detective, Mischievous, Sneaky, Cheeze, Suspense, Comedy','Oboe, Piano','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XCD0004_005_The+Best+Laid+Plans_Full+Version.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(76,'EXMX001900200','Light Head','Xseries','XCD019 Techno',2,'03:45','Full Version',24,'48','Electronic Dance Music',150,'Fast','Electronic, Party, Driving, Sports, Punchy, Energetic, Hypnotic, Dance, Futuristic','Synthesizer','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XCD0019_002_Light+Head_Full+Version.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(77,'EXTW000300100','Overture: The Marriage Of Figaro','Ultimate Classix','XCL003 Mozart Vol 1',1,'04:18','Full Version',24,'48','Classical',145,'Fast','Motivational, Thrilling, Staccato, Wedding, Sports, Anticipation, Builds, Swells','Flute, Oboe, Orchestra - Large','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XCL0003_001_Overture%3B+The+Marriage+Of+Figaro_Full+Version.MP3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(78,'EXTW000400300','The Four Seasons Concerto No. 1 In E Major (Spring) - Allegro Pastorale','Ultimate Classix','XCL004 Vivaldi Vol 1',3,'04:02','Full Version',24,'48','Classical',NULL,'Variable','Spring, Dynamic, Graceful, Lyrical, Wedding, Lively, Swirling, Shimmering','Harpsichord, Violin','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XCL0004_003_The+Four+Seasons+Concerto+No.+1+In+E+Major+(Spring)+-+Allegro+Pastorale_Full+Version.MP3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(79,'EXTS007800600','Symphony No. 94 In Surprise, Finale - Allegro Di Molto','Ultimate Classix','XCL078 Haydn Vol 2',6,'04:16','Full Version',24,'48','Classical',NULL,'Variable','Spring, Light, Energetic, Orchestral','Orchestra - Large','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XCL0078_006_Symphony+No.+94+In+Surprise%2C+Finale+-+Allegro+Di+Molto_Full+Version.MP3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(80,'EXTS008800500','Symphony No.103 - Adagio-Allegro Con Spirito','Ultimate Classix','XCL088 Haydn Vol 3',5,'09:25','Full Version',24,'48','Classical',NULL,'Variable','Danger, Dynamic, Intrigue, Ominous, Anticipation, Orchestral','Drum Roll, Orchestra - Large','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XCL0088_008_Symphony+No.103+-+Finale-Allegro+Con+Spirito_Full+Version.MP3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(81,'EXTS000200500','The Mark Of Torro','Easy Listening','XEL002 Grand Tourismo',5,'05:08','Full Version',24,'48','Easy Listening',73,'Fast','Bullfight, Heroic, Paso Doble, Macho, Retro, Europe, Spaghetti Western, Orchestral','Flute, Oboe','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XEL0002_005_The+Mark+Of+Torro_Full+Version.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(82,'EXTS002300200','Frisky Sour','Easy Listening','XEL023 Jet Set',2,'03:09','Full Version',24,'48','Easy Listening',163,'Fast','Kitsch, Quirky, Light, Talk Show, Thematic, Muzak, Energetic, Content, Stings','Brass, Jazz Guitar, Saxophone','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XEL0023_002_A_Frisky+Sour_Full+Version.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(83,'EXTS004100200','Hello Big Shot','Easy Listening','XEL041 Hello Big Shot',2,'03:16','Full Version',24,'48','Jazz',140,'Medium','Light, Flowing, Sneaky, Retro, Bouncy, Anticipation, Sweet, Dance, Crooner, Contemplative','Brass, Piano','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XEL0041_002_A_HELLO+BIG+SHOT_Main.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(84,'EXTS000700100','Brook Of Mormon','Earth Tones','XET007 Rivers',1,'03:13','Full Version',24,'48','Score',110,'Medium','Surreal, Discovery, Flowing, Soaring, Lush, Anticipation, Shimmering, Majestic, Swells, Wonderment, Contemplative','Brass, Strings','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XET0007_001_A_Brook+Of+Mormon_Full+Version.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(85,'EXTS000800200','Ecosystem Of A Down','Earth Tones','XET008 Forests',2,'03:11','Full Version',24,'48','Score',140,'Medium - Fast','Heroic, Discovery, Flowing, Panoramic, Anticipation, Shimmering, Majestic, Builds, Swells, Struggle, Magical, Wonderment','Strings','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XET0008_002_A_Ecosystem+Of+A+Down_Full+Version.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(86,'EXTS000200100','Turning My World Around','Grandmaster','XGM002 Hugh Padgham',1,'03:51','Full Version',24,'48','Rock',106,'Medium','Anthemic, Song, Melancholy, Edgy, Angst','Electric Guitar','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XGM0002_001_A_Turning+My+World+Around_Full+Version.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(87,'EXTS000700100','My Fallen Angel','Grandmaster','XGM007 Eddie Kramer',1,'03:15','Full Version',24,'48','Rock',121,'Medium','Psychedelia, Summer, Intrigue, Smooth, Driving, Song, Stoner, Retro, Lively, Punchy, Marijuana, Flower Power, Distorted, Swagger, Stings','Wah Wah Guitar','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XGM0007_001_A_My+Fallen+Angel_Full+Version.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(88,'EXUX000100100','Viscous Flow','Lab Rat Recordings','XLR001 Reactor 1',1,'03:56','Full Version',24,'48','Atmospheric',NULL,'No Tempo','Bizarre, Scary, Low','','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XLR0001_001_VISCOUS+FLOW.MP3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(89,'EXUX000200100','Relative Uncertainty','Lab Rat Recordings','XLR002 Reactor 2',1,'03:47','Full Version',24,'48','Atmospheric',NULL,'No Tempo','Floating, Surreal, Light, Electronic, Bright, Shimmering, Trippy, Islands','','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XLR0002_001_RELATIVE+UNCERTAINTY.MP3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(90,'EXUX010500200','I\'m A Believer','Mixtape','XMT105 Hip Hop Ambition',2,'03:57','Full Version',24,'48','Hip Hop',82,'Medium','Motivational, Groove, Electronic, Driving, Song, Powerful, Punchy, Soulful, Bright, Hook, Catchy','Synthesizer','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XMT0105_002_A_I\'m+A+Believer_Full.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(91,'EXUX010900800','Feels Like This','Mixtape','XMT109 Indie Rock',8,'03:10','Full Version',24,'48','Rock',160,'Fast','Heroic, Anthemic, Driving, Song, Lively, Bouncy, Alternative, Powerful, Punchy, Energetic, Anticipation, Catchy','Electric Guitar, Pad','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XMT0109_008_A_Feels+Like+This_Full.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(92,'EXXS000200200','Je Crois Entendre Encore','Ultimate Classix','XOP002 Tenor',2,'03:48','Full Version',24,'48','Classical',NULL,'Variable','Light, Gentle, Soaring, Swells','Orchestra - Large','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XOP0002_002_A_Je+Crois+Entendre+Encore_Full+Version.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(93,'EXXS000600200','Non Piu Andrai','Ultimate Classix','XOP006 Bass',2,'04:10','Full Version',24,'48','Classical',NULL,'Variable','Lively, Soaring, Comedy','Orchestra - Large','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XOP0006_002_A_Non+Piu+Andrai_Full+Version.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(94,'EXXS016400100','La Tarantella','Passport','XPS164 Passport To Italy 2',1,'03:28','Full Version',24,'48','World',120,'Medium','Mafia, Lively, Bouncy, Travel, Anticipation, Dance','Accordion, Fiddle, Mandolin','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XPS0164_001_A_La+Tarantella_Full+Version.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(95,'EXXS000900100','Junk In The Trunk','Reality Check','XRC009 Backstories',1,'03:09','Full Version',24,'48','Reality',100,'Medium','Light, Electronic, Builds, Contemplative','Piano','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XRC0009_001_A_Junk+In+The+Trunk_Full.MP3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(96,'EXXS000200900','In Your Face','Reality Check','XRC002 Backstabbing',9,'02:58','Full Version',24,'48','Reality',138,'Medium','Dynamic, Intrigue, Driving, Suspense, Punchy, Builds, Orchestral','Brass, Strings','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XRC0002_009_A_In+Your+Face_Full.MP3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(97,'EXXS000500400','Bite Yer Arm Off','Spitfire Audio','XSF005 Argy Bargy',4,'02:21','Full Version',24,'48','Score',180,'Fast','Danger, Epic, Dynamic, Electronic, Intrigue, Pursuit, Big, Suspense, Intense, Ominous, Powerful, Punchy, Anticipation, Builds, Struggle, Distorted, Boomy, Badass','Drums, Strings','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XSF0005_004_A_Bite+Yer+Arm+Off_Full+Version.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(98,'EXXS000100100','Angry Young Men','Super Rock','XSR001 Angry Young Men',1,'04:05','Full Version',24,'48','Rock',119,'Medium','Driving, Song, Punchy, Angst, Catchy','Electric Guitar','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XSR0001_001_A_Angry+Young+Men_Full.MP3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(99,'EXXS000400100','The Kind Of Man','Stampede','XST004 Sad To The Bone',1,'04:16','Full Version',24,'48','Country',57,'Slow','Lonely, Song, Southern, Hook, Slow Dance, Catchy, Twang','Acoustic Guitar, Piano, Lap Steel Guitar','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XST0004_001_A_The+Kind+Of+Man_Full+Version.mp3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL),
	(100,'EXXS000501300','Victorious Bastards','The 13 Brotherhood','XXL005 Shift Of Power',13,'03:42','Full Version',24,'48','Score',NULL,'Variable','Heavy, Robotic, Electronic, Pursuit, Thrilling, Intense, Futuristic','Synthesizer, Fx','https://tempd.s3.ap-northeast-2.amazonaws.com/EXTREME/XXL005_13+VICTORIOUS+BASTARDS.MP3',NULL,1,10000,NULL,NULL,'50',NULL,0,1,0,NULL,'2020-02-07 08:24:07','2020-02-07 08:24:07',NULL,NULL,NULL);

/*!40000 ALTER TABLE `tracks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_file_entry
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_file_entry`;

CREATE TABLE `user_file_entry` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `file_entry_id` int(10) unsigned NOT NULL,
  `owner` tinyint(1) NOT NULL DEFAULT '0',
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_file_entry_file_entry_id_user_id_unique` (`file_entry_id`,`user_id`),
  KEY `user_file_entry_user_id_index` (`user_id`),
  KEY `user_file_entry_file_entry_id_index` (`file_entry_id`),
  KEY `user_file_entry_owner_index` (`owner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table user_links
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_links`;

CREATE TABLE `user_links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_links_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table user_profiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_profiles`;

CREATE TABLE `user_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `header_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `header_colors` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_profiles_user_id_unique` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table user_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_group_user_id_group_id_unique` (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;

INSERT INTO `user_role` (`id`, `user_id`, `role_id`, `created_at`)
VALUES
	(1,1,2,'2020-02-06 16:43:28'),
	(2,2,2,NULL);

/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `legacy_permissions` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '1',
  `confirmation_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timezone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `available_space` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_api_token_unique` (`api_token`),
  KEY `users_username_index` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `first_name`, `last_name`, `avatar_url`, `gender`, `legacy_permissions`, `email`, `password`, `api_token`, `card_brand`, `card_last_four`, `remember_token`, `created_at`, `updated_at`, `confirmed`, `confirmation_code`, `language`, `country`, `timezone`, `avatar`, `stripe_id`, `available_space`)
VALUES
	(1,'root',NULL,NULL,NULL,NULL,NULL,'a.dewaskar@gmail.com','$2y$10$qtYnhJjKaYwfSCjxO/IAFuqhj9f/RTO.pUux1vR5Hy4RulfEf3JhC','L54LB8s4UKjSGD0svdDeGemkbZAvlLghMPoWmh9K',NULL,NULL,'DeTFEdLOcuuO7mK74uQPMNzHxKyIaQooin8ZnrPB9WqDQaYkuMKeTcTcFdjV','2020-02-06 16:43:27','2020-02-06 16:43:27',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,NULL,NULL,NULL,NULL,NULL,NULL,'aditya.dewaskar@razorpay.com','$2y$10$6KTVSyeN6wdiqSPhe4AU3e6ikd0Xx3jIlOvA8lOXDiVJfrE60qHNu',NULL,NULL,NULL,'BJdRKzR7kWYzmuGaQ1UNfHUAJWwo6B4K5itniOWEOswqtnfUgnNREDGfgixn','2020-02-06 18:44:03','2020-02-06 18:44:03',1,NULL,'english',NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users_oauth
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_oauth`;

CREATE TABLE `users_oauth` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `service` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_oauth_user_id_service_unique` (`user_id`,`service`),
  UNIQUE KEY `users_oauth_token_unique` (`token`),
  KEY `users_oauth_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
