<?php namespace App\Http\Controllers;

use App;
use App\Album;
use App\Artist;
use App\Services\Artists\NormalizesArtist;
use App\Services\Providers\Local\LocalSearch;
use App\Services\Providers\ProviderResolver;
use App\Services\Search\PlaylistSearch;
use App\Services\Search\SearchSaver;
use App\Services\Search\UserSearch;
use App\Track;
use App\User;
use Common\Core\BaseController;
use Common\Settings\Settings;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class SearchController extends BaseController
{
    use NormalizesArtist;

    /**
     * @var ProviderResolver
     */
    private $provider;

    /**
     * @var SearchSaver
     */
    private $saver;

    /**
     * @var UserSearch
     */
    private $userSearch;

    /**
     * @var PlaylistSearch
     */
    private $playlistSearch;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Settings
     */
    private $settings;

    /**
     * @param Request $request
     * @param Settings $settings
     * @param SearchSaver $saver
     * @param UserSearch $userSearch
     * @param ProviderResolver $provider
     * @param PlaylistSearch $playlistSearch
     */
    public function __construct(
        Request $request,
        SearchSaver $saver,
        Settings $settings,
        UserSearch $userSearch,
        ProviderResolver $provider,
        PlaylistSearch $playlistSearch
    )
    {
        $this->saver = $saver;
        $this->request = $request;
        $this->settings = $settings;
        $this->provider = $provider;
        $this->userSearch = $userSearch;
        $this->playlistSearch = $playlistSearch;
    }

    /**
     * Use active search provider to search for
     * songs, albums and artists matching given query.
     *
     * @param string $q
     * @return JsonResponse
     */
    public function index($q)
    {
        $this->authorize('show', Artist::class);
        $this->authorize('show', Track::class);

        $limit = $this->request->get('limit', 3);
        $contentProvider = $this->provider->get('search', $this->request->get('forceLocal') ? 'local' : null);

        $results = $contentProvider->search($q, $limit);

        if ( ! is_a($contentProvider, LocalSearch::class) ) {
            $results = $this->saver->save($results);
        }

        $results['playlists'] = $this->playlistSearch->search($q, $limit);
        $results['users'] = $this->userSearch->search($q, $limit);

        if ($this->request->get('searchChannels')) {
            $results['channels'] = app(App\Channel::class)
                ->where('name', 'like', "$q%")
                ->limit($limit)
                ->get();

            $results['genres'] = app(App\Genre::class)
                ->where('name', 'like', "$q%")
                ->limit($limit)
                ->get();
        }


        $results = $this->filterOutBlockedArtists($results);
        $results['artists'] = $results['artists']->map(function($artist) {
            return $this->normalizeArtist($artist);
        });

        $response = [
            'query' => e($q),
            'results' =>  $this->filterOutBlockedArtists($results),
        ];

        if ($this->request->get('flatten')) {
            $response['results'] = Arr::flatten($response['results'], 1);
        }

        return $this->success($response);
    }

    /**
     * @param int $trackId
     * @param string $artistName
     * @param string $trackName
     * @return array
     */
    public function searchAudio($trackId, $artistName, $trackName)
    {
        $this->authorize('show', Track::class);

        return $this->provider->get('audio_search')->search($trackId, $artistName, $trackName, 1);
    }

    /**
     * @return JsonResponse
     */
    public function searchLocalArtists()
    {
        $this->authorize('show', Artist::class);

        $query = $this->request->get('query');
        $limit = $this->request->get('limit', 6);

        $artists = Artist::where('name', 'like', "$query%")
            ->orderByPopularity('desc')
            ->limit($limit)
            ->select(['id', 'name', 'image_small'])
            ->get();

        $users = app(User::class)
            ->where('email', 'like', "$query%")
            ->orWhere('first_name', 'like', "$query%")
            ->orWhere('last_name', 'like', "$query%")
            ->limit($limit)
            ->get();

        $normalizedResults = $artists->concat($users)->slice(0, $limit)->map(function($model) {
            return $this->normalizeArtist($model);
        });

        return $this->success(['results' => $normalizedResults]);
    }

    /**
     * @return JsonResponse
     */
    public function searchLocalAlbums()
    {
        $this->authorize('show', Album::class);

        $query = $this->request->get('query');
        $limit = $this->request->get('limit', 7);

        $builder = app(Album::class)->with('artist')->limit($limit);
        if ($query) {
            $builder->where('name', 'like', "$query%");
        }
        $albums = $builder->get();

        return $this->success(['results' => $albums]);
    }

    /**
     * Remove artists that were blocked by admin from search results.
     *
     * @param array $results
     * @return array
     */
    private function filterOutBlockedArtists($results)
    {
        if (($artists = $this->settings->get('artists.blocked'))) {
            $artists = json_decode($artists);

            foreach ($results['artists'] as $k => $artist) {
                if ($this->shouldBeBlocked($artist['name'], $artists)) {
                    unset($results['artists'][$k]);
                }
            }

            foreach ($results['albums'] as $k => $album) {
                if (isset($album['artist'])) {
                    if ($this->shouldBeBlocked($album['artist']['name'], $artists)) {
                        unset($results['albums'][$k]);
                    }
                }
            }

            foreach ($results['tracks'] as $k => $track) {
                if (isset($track['album']['artist'])) {
                    if ($this->shouldBeBlocked($track['album']['artist']['name'], $artists)) {
                        unset($results['tracks'][$k]);
                    }
                }
            }
        }

        return $results;
    }

    /**
     * Check if given artist should be blocked.
     *
     * @param string $name
     * @param array $toBlock
     * @return boolean
     */
    private function shouldBeBlocked($name, $toBlock)
    {
        foreach ($toBlock as $blockedName) {
            $pattern = '/' . str_replace('*', '.*?', strtolower($blockedName)) . '/i';
            if (preg_match($pattern, $name)) return true;
        }
    }
}
