<?php

namespace App\Http\Controllers;

use App\Album;
use App\Artist;
use App\Services\Artists\NormalizesArtist;
use Carbon\Carbon;
use Common\Core\BaseController;
use Common\Files\Actions\CreateFileEntry;
use Common\Files\Actions\Storage\StorePublicUpload;
use Common\Files\FileEntry;
use Common\Files\Traits\GetsEntryTypeFromMime;
use getID3;
use getid3_lib;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class MusicUploadController extends BaseController
{
    use GetsEntryTypeFromMime, NormalizesArtist;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Artist
     */
    private $artist;

    /**
     * @var Album
     */
    private $album;

    /**
     * @param Request $request
     * @param Artist $artist
     * @param Album $album
     */
    public function __construct(Request $request, Artist $artist, Album $album)
    {
        $this->request = $request;
        $this->artist = $artist;
        $this->album = $album;
    }

    public function upload()
    {
        $this->authorize('store', FileEntry::class);

        $this->validate($this->request, [
            'file' => 'required|file'
        ]);

        $fileEntry = $this->storePublicFile();
        $autoMatch = filter_var($this->request->get('autoMatch'), FILTER_VALIDATE_BOOLEAN);

        $getID3 = new getID3;
        $path = $fileEntry->getStoragePath();
        $metadata = $getID3->analyze(public_path("storage/$path"));
        getid3_lib::CopyTagsToComments($metadata);

        $normalizedMetadata = array_map(function($item) {
            return $item && is_array($item) ? array_first($item) : $item;
        }, Arr::get($metadata, 'comments', []));

        // store thumbnail
        if (isset($normalizedMetadata['picture'])) {
            $normalizedMetadata = $this->storeMetadataPicture($normalizedMetadata);
        }

        if (isset($metadata['playtime_seconds'])) {
            $normalizedMetadata['duration'] = floor($metadata['playtime_seconds']) * 1000;
        }

        if (isset($normalizedMetadata['genre'])) {
            $normalizedMetadata['genres'] = explode(',', $normalizedMetadata['genre']);
            unset($normalizedMetadata['genre']);
        }

        if (isset($normalizedMetadata['artist'])) {
            $normalizedMetadata['artist_name'] = $normalizedMetadata['artist'];
            unset($normalizedMetadata['artist']);
            if ($autoMatch) {
                $normalizedMetadata['artist'] = $this->artist->firstOrCreate(['name' => $normalizedMetadata['artist_name']]);
                if ($normalizedMetadata['artist']) {
                    $normalizedMetadata['artist'] = $this->normalizeArtist($normalizedMetadata['artist']);
                }
            }
        }

        if (isset($normalizedMetadata['album'])) {
            $normalizedMetadata['album_name'] = $normalizedMetadata['album'];
            unset($normalizedMetadata['album']);
            if ($autoMatch) {
                $normalizedMetadata['album'] = $this->album->where('name', $normalizedMetadata['album_name'])->first();
            }
        }

        if (isset($normalizedMetadata['date'])) {
            $normalizedMetadata['release_date'] = Carbon::parse($normalizedMetadata['date'])->toDateString();
            unset($normalizedMetadata['date']);
        }

        if ( ! isset($normalizedMetadata['title'])) {
            $name = pathinfo($fileEntry->name, PATHINFO_FILENAME);
            $normalizedMetadata['title'] = Str::title($name);
        }

        return $this->success(['fileEntry' => $fileEntry, 'metadata' => $normalizedMetadata], 201);
    }

    /**
     * @param array $normalizedMetadata
     * @return array
     */
    private function storeMetadataPicture($normalizedMetadata)
    {
        $mime = $normalizedMetadata['picture']['image_mime'];
        $fileData = [
            'name' => 'thumbnail.png',
            'file_name' => str_random(40),
            'mime' => $mime,
            'type' => $this->getTypeFromMime($mime),
            'file_size' => $normalizedMetadata['picture']['datalength'],
            'extension' => last(explode('/', $mime)),
        ];

        $fileEntry = app(CreateFileEntry::class)->execute($fileData, ['public_path' => 'track_image']);
        app(StorePublicUpload::class)->execute($fileEntry, $normalizedMetadata['picture']['data']);
        unset($normalizedMetadata['picture']);
        $normalizedMetadata['image'] = $fileEntry;
        return $normalizedMetadata;
    }

    /**
     * @return FileEntry
     */
    private function storePublicFile()
    {
        $uploadFile = $this->request->file('file');
        $publicPath = 'track_media';

        $fileEntry = app(CreateFileEntry::class)
            ->execute($uploadFile, ['public_path' => $publicPath]);

        app(StorePublicUpload::class)->execute($fileEntry, $uploadFile);

        return $fileEntry;
    }
}
