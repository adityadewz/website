<?php

namespace App\Http\Controllers;

use App\Track;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Common\Core\BaseController;
use Common\Settings\Settings;

class DownloadLocalTrackController extends BaseController
{
    /**
     * @var Track
     */
    private $track;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Settings
     */
    private $settings;

    /**
     * @param Track $track
     * @param Request $request
     * @param Settings $settings
     */
    public function __construct(Track $track, Request $request, Settings $settings)
    {
        $this->track = $track;
        $this->request = $request;
        $this->settings = $settings;
    }

    public function download($id) {
        $track = $this->track->findOrFail($id);

        $this->authorize('download', $track);

        if ( ! $track->url) abort(404);

        $ext = pathinfo($track->url, PATHINFO_EXTENSION);
        $trackName = str_replace('%', '', Str::ascii($track->name)).".$ext";

        return response()->download($track->url, $trackName);
    }
}
