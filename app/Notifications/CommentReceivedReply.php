<?php

namespace App\Notifications;

use App\Track;
use App\User;
use Common\Comments\Comment;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CommentReceivedReply extends Notification
{
    use Queueable;

    /**
     * @var array
     */
    public $newComment;

    /**
     * @var array
     */
    private $originalComment;

    /**
     * @var array
     */
    private $track;

    /**
     * @param Comment $newComment
     * @param array $originalComment
     */
    public function __construct($newComment, $originalComment)
    {
        $this->newComment = $newComment;
        $this->originalComment = $originalComment;
        $track = app(Track::class)
            ->select(['name', 'id'])
            ->find($newComment['commentable_id']);
        $this->track = ['name' => $track->name, 'id' => $track->id];
    }

    /**
     * @param User $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * @param User $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * @param User $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'comment_content' => str_limit($this->newComment['content'], 180),
            'track' => $this->track,
            'user' => [
                'id' => $this->originalComment['user']['id'],
                'avatar' => $this->originalComment['user']['avatar'],
                'display_name' => $this->originalComment['user']['display_name'],
                'model_type' => $this->originalComment['user']['model_type'],
            ],
        ];
    }
}
