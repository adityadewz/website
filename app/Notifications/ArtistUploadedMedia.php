<?php

namespace App\Notifications;

use App\Album;
use App\Services\UrlGenerator;
use App\Track;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Arr;

class ArtistUploadedMedia extends Notification
{
    use Queueable;

    /**
     * @var array
     */
    private $data;

    /**
     * @param Album|Track $media
     */
    public function __construct($media)
    {
        if (is_a($media, Track::class)) {
            $media->load('artists', 'album');
            $this->data = [
                'media' => [
                    'model_type' => $media['model_type'],
                    'id' => $media['id'],
                    'name' => $media['name'],
                    'image' => $media['image'] || Arr::get($media, 'album.image'),
                ],
                'artist' => [
                    'id' => $media['artists'][0]['id'],
                    'name' => $media['artists'][0]['name'],
                ]
            ];
        } else {
            $media->load('artist');
            $this->data = [
                'media' => [
                    'model_type' => $media['model_type'],
                    'name' => $media['name'],
                    'image' => $media['image'],
                    'id' => $media['id'],
                ],
                'artist' => [
                    'id' => $media['artist']['id'],
                    'name' => $media['artist']['display_name'],
                ]
            ];
        }
    }

    /**
     * @param  User  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * @param  User  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $modelType = $this->data['media']['model_type'];
        $data = [
            'artist' => $this->data['artist']['name'],
            'mediaType' => __(strtolower(class_basename($modelType))),
        ];

        $url = $modelType === Track::class ?
            app(UrlGenerator::class)->track($this->data['media']) :
            app(UrlGenerator::class)->album($this->data['media']);

        return (new MailMessage)
                    ->line(__(':artist just uploaded a new :mediaType', $data))
                    ->action(__('View Now'), $url);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->data;
    }
}
