<?php

namespace App\Services\Tracks\Queries;

use App\Artist;
use App\Track;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AlbumTrackQuery extends BaseTrackQuery
{
    const ORDER_COL = 'number';
    const ORDER_DIR = 'asc';

    public function get($albumId)
    {
        return $this->baseQuery()
            ->where('tracks.album_id', $albumId);
    }
}