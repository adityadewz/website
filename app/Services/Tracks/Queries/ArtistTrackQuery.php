<?php

namespace App\Services\Tracks\Queries;

use App\Traits\DeterminesArtistType;

class ArtistTrackQuery extends BaseTrackQuery
{
    use DeterminesArtistType;

    const ORDER_COL = 'spotify_popularity';

    public function get($artistId)
    {
        return $this->baseQuery()
            ->join('artist_track', 'tracks.id', '=', 'artist_track.track_id')
            ->where('artist_track.artist_id', $artistId)
            ->where('artist_track.artist_type', $this->determineArtistType())
            ->select('tracks.*');
    }
}