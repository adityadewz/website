<?php namespace App\Services\Providers\Local;

use App\Album;
use App\Track;
use App\Artist;
use App\Services\Search\SearchInterface;
use App\User;
use Common\Settings\Settings;
use Illuminate\Database\Eloquent\Builder;

class LocalSearch implements SearchInterface {

    /**
     * @param string $q
     * @param int $limit
     * @return array
     */
    public function search($q, $limit = 10) {
        $q = urldecode($q);

        return [
            'artists' => $this->findArtists($q, $limit),
            'albums'  => Album::with('artist')->where('name' ,'like', $q.'%')->limit($limit)->get(),
            'tracks'  => Track::with('album', 'artists')->where('name', 'like', $q.'%')->limit($limit)->get()
        ];
    }

    private function findArtists($q, $limit)
    {
        if (app(Settings::class)->get('player.artist_type') === 'user') {
            return app(User::class)
                ->where('username', 'like', $q.'%')
                ->whereHas('uploadedTracks')
                ->limit($limit)
                ->get();
        } else {
            return Artist::where('name', 'like', $q.'%')->limit($limit)->get();
        }
    }
}